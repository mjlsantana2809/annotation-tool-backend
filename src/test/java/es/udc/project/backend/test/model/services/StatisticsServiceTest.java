package es.udc.project.backend.test.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.PermissionDao;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Psychological_Analysis.Pathology;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;
import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.services.AnalysisService;
import es.udc.project.backend.model.services.CollectionService;
import es.udc.project.backend.model.services.Statistics;
import es.udc.project.backend.model.services.StatisticsService;
import es.udc.project.backend.model.services.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class StatisticsServiceTest {
	
	private final Long NON_EXISTENT_ID = new Long(-1);
	
	@Autowired
	private StatisticsService statisticsService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AnalysisService analysisService;
	
	@Autowired
	private Psychological_AnalysisDao psycologicalAnalysisDao;
	
	@Autowired
	private WritingDao writingDao;
	
	
	@Autowired
	private CollectionService collectionService;
	@Test
	public void countByPathology() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		Writing writing2= writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text2", "author2"));
		Writing writing3= writingDao.save(new Writing(collection, "title3", LocalDateTime.now(), "info", "text3", "author1"));
		Writing writing4= writingDao.save(new Writing(collection, "title4", LocalDateTime.now(), "info", "text4", "author2"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		psycologicalAnalysisDao.save(new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "", false, user, writing1, Pathology.DETECTED));
		psycologicalAnalysisDao.save(new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "", false, user, writing2, Pathology.DETECTED));
		psycologicalAnalysisDao.save(new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "", false, user, writing3, Pathology.MORE_INFO_NEEDED));
		psycologicalAnalysisDao.save(new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "", false, user, writing4, Pathology.NONE));
		Statistics statistics= statisticsService.countByPathology(null);
		
		List<Object[]> data = statistics.getData();
		
		for(Object[] result:data) {
			Pathology pathology= (Pathology) result[0];
			long count= ((Number) result[1]).longValue();
			switch(pathology.ordinal()) {
				case 0:
					assertEquals((long)1,count);
					break;
				case 1:
					assertEquals((long)2,count);
					break;
				default:
					assertEquals((long)1,count);
					break;
			}
			
		}
		assertEquals((long)4,statistics.getTotal());
	}
}
