package es.udc.project.backend.test.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.Methodology;
import es.udc.project.backend.model.entities.MethodologyDao;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.PermissionDao;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;
import es.udc.project.backend.model.entities.Relation_Type;
import es.udc.project.backend.model.entities.Relation_TypeDao;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.model.entities.Speech_AnalysisDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadySharedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.model.services.AnalysisService;
import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.services.CollectionService;
import es.udc.project.backend.model.services.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class AnalysisServiceTest {
	
	private final Long NON_EXISTENT_ID = new Long(-1);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CollectionService collectionService;
	
	@Autowired
	private AnalysisService analysisService;
	
	@Autowired
	private Psychological_AnalysisDao psycologicalAnalysisDao;
	
	@Autowired
	private Relation_TypeDao relationDao;
	
	@Autowired
	private WritingDao writingDao;
	
	@Autowired
	private Speech_AnalysisDao speechAnalysisDao;
	
	@Autowired
	private MethodologyDao methodologyDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	private Collection createCollectionWithSomePosts() {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		
		Iterable<Writing> writings=writingDao.saveAll(Arrays.asList(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1")
					,new Writing(collection, "title2", LocalDateTime.now(), "info", "text2", "author1")
					,new Writing(collection, "title3", LocalDateTime.now(), "info", "text3", "author1")
					,new Writing(collection, "title4", LocalDateTime.now(), "info", "text4", "author2")));
		List<Writing> writingList = new ArrayList<>();
		writings.forEach(writingList::add);
		collection.setWritings(writingList);
		return collection;
	}
	
	
	
	@Test
	public void startPsychologicalAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Psychological_Analysis found=psycologicalAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId())).get();
		assertEquals(writing1,found.getWriting());
		assertEquals(user,found.getUser());
	}
	
	@Test
	public void startPsychologicalAnalysisTwice() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		analysisService.startAnalysis(writing1.getId(), user.getId());
		assertThrows(AnalysisAlreadyStartedException.class, () ->
		analysisService.startAnalysis(writing1.getId(), user.getId()));
	}
	
	@Test
	public void finishPsychologicalAnalysis() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Long analysisId= analysisService.startAnalysis(writing1.getId(), user.getId());
		analysisService.finishAnalysis(analysisId, user.getId());
		Optional<Psychological_Analysis> found= psycologicalAnalysisDao.findById(analysisId);
		assertTrue(found.get().isFinished());
	}
	
	@Test
	public void finishPsychologicalAnalysisWithNonExistentId() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException {
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.ADMIN,LocalDateTime.now());
		userService.signUp(user, false);
		
		
		assertThrows(InstanceNotFoundException.class, () ->
			analysisService.finishAnalysis(NON_EXISTENT_ID, user.getId()));
	}
	
	@Test
	public void finishPsychologicalAnalysisWithoutPermission() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		User user2 =new User("username2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user2, false);
		Psychological_Analysis analysis =psycologicalAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId())).get();
		assertThrows(PermissionException.class, () ->
			analysisService.finishAnalysis(analysis.getId(), user2.getId()));
	}
	@Test
	public void finishPsychologicalAnalysisTwice() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Psychological_Analysis analysis=psycologicalAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId())).get();
		analysisService.finishAnalysis(analysis.getId(), user.getId());
		assertThrows(AnalysisAlreadyFinishedException.class, () ->
		analysisService.finishAnalysis(analysis.getId(), user.getId()));
	}
	
	@Test
	public void continueAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user, false);
		User user2 =new User("username2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user2, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		collectionService.updatePermission(user2.getId(), collection.getId());
		Psychological_Analysis analysis=psycologicalAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId())).get();
		analysisService.continuePsychologicalAnalysis(writing1.getId(), user.getId(),null);
		assertEquals(analysis,analysisService.continuePsychologicalAnalysis(writing1.getId(), user.getId(),null));
		
		 
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Speech_Analysis speechAnalysis=speechAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user2.getId(),m.getId())).get();
		
		assertEquals(speechAnalysis,analysisService.continueSpeechAnalysis(writing1.getId(), user2.getId(),null).getAnalysis());
	}
	
	@Test
	public void startSpeechAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Speech_Analysis found=speechAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId(),m.getId())).get();
		assertEquals(writing1,found.getWriting());
		assertEquals(user,found.getUser());
	}
	
	@Test
	public void findMethodologies() {
		List<Methodology> expected = new ArrayList<Methodology>();
		expected.add(methodologyDao.save(new Methodology("methodology1", "description1")));
		expected.add(methodologyDao.save(new Methodology("methodology2", "description2")));
		assertEquals(expected,analysisService.findMethodologies());
	}
	
	
	@Test
	public void findRelations() {
		
		Methodology methodology =methodologyDao.save(new Methodology("name1", "description1"));
		List<Relation_Type> expected= new ArrayList<>();
		expected.add(relationDao.save( new Relation_Type( "name1", true, methodology)));
		expected.add(relationDao.save( new Relation_Type( "name2", true, methodology)));
		
		assertEquals(expected,analysisService.findRelations(methodology.getId()));
	}
	
	@Test
	public void finishSpeechAnalysis() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId= analysisService.startAnalysis(writing1.getId(), user.getId(), m.getId());
		analysisService.finishAnalysis(analysisId, user.getId());
		Optional<Speech_Analysis> found= speechAnalysisDao.findById(analysisId);
		assertTrue(found.get().isFinished());
	}
	
	@Test
	public void finishSpeechAnalysisWithNonExistentId() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException {
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.ADMIN,LocalDateTime.now());
		userService.signUp(user, false);
		
		
		assertThrows(InstanceNotFoundException.class, () ->
			analysisService.finishAnalysis(NON_EXISTENT_ID, user.getId()));
	}
	
	@Test
	public void finishSpeechAnalysisWithoutPermission() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		User user2 =new User("username2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user2, false);
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Speech_Analysis analysis =speechAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId(), m.getId())).get();
		assertThrows(PermissionException.class, () ->
			analysisService.finishAnalysis(analysis.getId(), user2.getId()));
	}
	@Test
	public void finishSpeechAnalysisTwice() throws InstanceNotFoundException, DuplicatedInstanceException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Speech_Analysis analysis=speechAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user.getId(), m.getId())).get();
		analysisService.finishAnalysis(analysis.getId(), user.getId());
		assertThrows(AnalysisAlreadyFinishedException.class, () ->
		analysisService.finishAnalysis(analysis.getId(), user.getId()));
	}
	
	@Test
	public void findAnalysisByUser() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadyFinishedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		Writing writing2= writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text1", "author1"));
		User user1 =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user1, false);
		User user2 =new User("username2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user2, false);
		collectionService.updatePermission(user1.getId(), collection.getId());
		collectionService.updatePermission(user2.getId(), collection.getId());
		
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Speech_Analysis speechAnalysis1=speechAnalysisDao.findById( analysisService.startAnalysis(writing1.getId(), user1.getId(), m.getId())).get();
		Speech_Analysis speechAnalysis2=speechAnalysisDao.findById( analysisService.startAnalysis(writing2.getId(), user1.getId(), m.getId())).get();
		analysisService.finishAnalysis(speechAnalysis1.getId(), user1.getId());
		Psychological_Analysis pAnalysis1 = psycologicalAnalysisDao.findById( analysisService.startAnalysis(writing2.getId(), user2.getId())).get();
		
		assertEquals(new Block<>(Arrays.asList(speechAnalysis1,speechAnalysis2),false),analysisService.findSpeechAnalysisByUser(user1.getId(), true,0,10)) ;
		assertEquals(new Block<>(Arrays.asList(pAnalysis1),false),analysisService.findPsychologicalAnalysisByUser(user2.getId(), true,0,10)) ;
		
		
		assertEquals(new Block<>(Arrays.asList(speechAnalysis2),false),analysisService.findSpeechAnalysisByUser(user1.getId(), false,0,10)) ;
	}
	
	@Test
	public void shareAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadySharedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser =new User("targetUser", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId = analysisService.startAnalysis(writing.getId(), user.getId(), m.getId());
		Permission permission =permissionDao.findById(analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), analysisId)).get();
		
		assertEquals(analysisId, permission.getSpeechAnalysis().getId());
		assertEquals(null, permission.getPsychologicalAnalysis());
		assertEquals("READ", permission.getType());
		
		User user2 =new User("username2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(user2, false);
		User targetUser2 =new User("targetUser2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(targetUser2, false);
		collectionService.updatePermission(user2.getId(), collection.getId());
		
		Long analysisId2 = analysisService.startAnalysis(writing.getId(), user2.getId());
		Permission permission2 =permissionDao.findById(analysisService.shareAnalysis(user2.getId(), targetUser2.getUserName(), analysisId2)).get();
		
		assertEquals(analysisId2, permission2.getPsychologicalAnalysis().getId());
		assertEquals(null, permission2.getSpeechAnalysis());
		assertEquals("READ", permission2.getType());
	}
	
	@Test
	public void shareAnalysisWithDifferentRoles() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser =new User("targetUser", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.PSYCHOLOGIST,LocalDateTime.now());
		userService.signUp(targetUser, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId = analysisService.startAnalysis(writing.getId(), user.getId(), m.getId());
		
		
		assertThrows(PermissionException.class, () ->
				analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), analysisId));
	}
	
	@Test
	public void shareAnalysisNonExistentAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser =new User("targetUser", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser, false);
		
		
		
		assertThrows(InstanceNotFoundException.class, () ->
				analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), NON_EXISTENT_ID));
	}
	
	@Test
	public void unshareAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadySharedException {
		Collection collection=collectionService.createCollection("analysisTestCollection");
		Writing writing= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser =new User("targetUser", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId = analysisService.startAnalysis(writing.getId(), user.getId(), m.getId());
		Long permissionId = analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), analysisId);
		
		assertTrue(permissionDao.findById(permissionId).isPresent());
		analysisService.unshareAnalysis(user.getId(), analysisId, targetUser.getId());
		assertFalse(permissionDao.findById(permissionId).isPresent());
		
		assertThrows(InstanceNotFoundException.class, () ->
			analysisService.unshareAnalysis(user.getId(), analysisId, targetUser.getId()));
		
		
	}
	@Test
	public void findSharedAnalysisTo() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadySharedException{
		Collection collection=collectionService.createCollection("analysisTestCollection");
		//3 analisis, solo uso 2
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		Writing writing2= writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text2", "author2"));
		writingDao.save(new Writing(collection, "title3", LocalDateTime.now(), "info", "text3", "author2"));
		
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser =new User("targetUser", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId1 = analysisService.startAnalysis(writing1.getId(), user.getId(), m.getId());
		Long analysisId2 = analysisService.startAnalysis(writing2.getId(), user.getId(), m.getId());
		
		Permission permission1 =permissionDao.findById( analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), analysisId1)).get();
		Permission permission2 =permissionDao.findById( analysisService.shareAnalysis(user.getId(), targetUser.getUserName(), analysisId2)).get();
		
		assertEquals(Arrays.asList(permission1,permission2),analysisService.findSharedAnalysisTo(targetUser.getId(),0,10).getItems());
	}
	
	@Test
	public void findMySharedAnalysis() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, AnalysisAlreadySharedException{
		Collection collection=collectionService.createCollection("analysisTestCollection");
		
		Writing writing1= writingDao.save(new Writing(collection, "title1", LocalDateTime.now(), "info", "text1", "author1"));
		Writing writing2= writingDao.save(new Writing(collection, "title2", LocalDateTime.now(), "info", "text2", "author2"));
		writingDao.save(new Writing(collection, "title3", LocalDateTime.now(), "info", "text3", "author2"));
		
		User user =new User("username1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(user, false);
		User targetUser1 =new User("targetUser1", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser1, false);
		User targetUser2 =new User("targetUser2", "firstName", "lastName", "testannotationtoolapp@gmail.com", RoleType.LINGUIST,LocalDateTime.now());
		userService.signUp(targetUser2, false);
		collectionService.updatePermission(user.getId(), collection.getId());
		Methodology m =methodologyDao.save(new Methodology("name1", "description1"));
		Long analysisId1 = analysisService.startAnalysis(writing1.getId(), user.getId(), m.getId());
		Long analysisId2 = analysisService.startAnalysis(writing2.getId(), user.getId(), m.getId());
		
		Permission permission1 =permissionDao.findById( analysisService.shareAnalysis(user.getId(), targetUser1.getUserName(), analysisId1)).get();
		Permission permission2 =permissionDao.findById( analysisService.shareAnalysis(user.getId(), targetUser1.getUserName(), analysisId2)).get();
		Permission permission3 =permissionDao.findById( analysisService.shareAnalysis(user.getId(), targetUser2.getUserName(), analysisId2)).get();
		
		assertEquals(Arrays.asList(permission1,permission2,permission3),analysisService.findMySharedAnalysis(user.getId(),0,10).getItems());
	}
	
	
	
}
