package es.udc.project.backend.test.model.services;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.services.CollectionService;
import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;
import es.udc.project.backend.model.entities.Individual;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;


@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class XmlConversorTest {

	@Autowired
	private CollectionService xmlConversor;


	@Autowired
	private WritingDao writingDao;
	@Autowired
	private CollectionDao collectionDao;

	@Test
	public void testParseAllFiles() throws IOException {

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("xmlColection").getFile());
		xmlConversor.parseAllFiles(file.getAbsolutePath());
		Slice<Writing> writingList2=writingDao.findByAuthor("test_subject2",PageRequest.of(0, 10));
		Slice<Writing> writingList3=writingDao.findByAuthor("test_subject3",PageRequest.of(0, 10));
		assertEquals(writingList2.getContent().size(), 2);
		assertEquals(writingList3.getContent().size(), 1);
		assertEquals(writingDao.count(), 3);

	}

}
