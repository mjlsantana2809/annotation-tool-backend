package es.udc.project.backend.test.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import es.udc.project.backend.model.exceptions.IncorrectLoginException;
import es.udc.project.backend.model.exceptions.IncorrectPasswordException;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PasswordNotStrongException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.services.UserService;


@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class UserServiceTest {

	private final Long NON_EXISTENT_ID = new Long(-1);

	@Autowired
	private UserService userService;




	private User createAdmin(String userName) {
		
		return new User(userName, "firstName", "lastName", "testannotationtoolapp@gmail.com",
				RoleType.ADMIN,LocalDateTime.now());
	}
	
	
	private User createExpert(String userName, int cont) {
		Random rd = new Random();
		if(rd.nextBoolean()) {
			return new User(userName+cont, "firstName", "lastName", "testannotationtoolapp@gmail.com",
					RoleType.LINGUIST,LocalDateTime.now());
		}else {
			return new User(userName+cont, "firstName", "lastName", "testannotationtoolapp@gmail.com",
					RoleType.PSYCHOLOGIST,LocalDateTime.now());
		}
		
	}
	

	
	@Test
	public void testSignUpAndLoginFromId() throws DuplicatedInstanceException, InstanceNotFoundException{
		User user = createAdmin("user");

		userService.signUp(user,false);
		User loggedInUser = userService.loginFromId(user.getId());
		Properties prop= System.getProperties();
		assertEquals(user, loggedInUser);

	}
	@Test
	public void testLoginFromNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () -> userService.loginFromId(NON_EXISTENT_ID));
	}
	
	@Test
	public void testLogin() throws DuplicatedInstanceException, IncorrectLoginException {
		
		User user = createExpert("user",1);
		String clearPassword = user.getPassword();
				
		userService.signUp(user,false);
		
		User loggedInUser = userService.login(user.getUserName(), clearPassword);
		
		assertEquals(user, loggedInUser);
		
	}
	
	@Test
	public void testLoginWithIncorrectPassword() throws DuplicatedInstanceException {
		
		User user = createExpert("user",1);
		String clearPassword = user.getPassword();
		
		userService.signUp(user,false);
		assertThrows(IncorrectLoginException.class, () ->
			userService.login(user.getUserName(), 'X' + clearPassword));
		
	}
	
	@Test
	public void testLoginWithNonExistentUserName() {
		assertThrows(IncorrectLoginException.class, () -> userService.login("X", "Y"));
	}
	
	
	@Test
	public void testChangePassword() throws DuplicatedInstanceException, InstanceNotFoundException,
		IncorrectPasswordException, IncorrectLoginException, PasswordNotStrongException {
		
		User user = createExpert("user",1);
		String oldPassword = user.getPassword();
		String newPassword = "NewPassword123!";
		
		userService.signUp(user,false);
		userService.changePassword(user.getId(), oldPassword, newPassword);
		userService.login(user.getUserName(), newPassword);
		
	}
	
	@Test
	public void testChangePasswordWithNonExistentId() {
		assertThrows(InstanceNotFoundException.class, () ->
			userService.changePassword(NON_EXISTENT_ID, "X", "Y"));
	}
	
	@Test
	public void testChangePasswordWithIncorrectPassword() throws DuplicatedInstanceException {
		
		User user = createExpert("user",1);
		String oldPassword = user.getPassword();
		String newPassword = 'X' + oldPassword;
		
		userService.signUp(user,false);
		assertThrows(IncorrectPasswordException.class, () ->
			userService.changePassword(user.getId(), 'Y' + oldPassword, newPassword));
		
	}
	
	@Test
	public void findExperts() throws DuplicatedInstanceException, InstanceNotFoundException, PermissionException  {
		List<User> expected = new ArrayList<User>();
		User user;
		for(int i=1; i<=5;i++) {
			user =createExpert("user",i);
			userService.signUp(user,false);
			expected.add(user);
		}
		User admin1=createAdmin("admin1");
		userService.signUp(admin1,false);
		User admin2=createAdmin("admin2");
		userService.signUp(admin2,false);
		assertEquals(expected,userService.findUsers(admin1.getId(),0,10).getItems());
		
	}
	
	@Test
	public void testUpdateProfile() throws InstanceNotFoundException, DuplicatedInstanceException {
		
		User user = createExpert("user",1);
		
		userService.signUp(user,false);
		
		user.setFirstName("(Updated)" + user.getFirstName());
		user.setLastName("(Updated)"  + user.getLastName());
		user.setEmail("(Updated)"  + user.getEmail());
		
		userService.updateProfile(user.getId(), "(Updated)"  + user.getFirstName(), "(Updated)"  + user.getLastName(),
				"(Updated)"  + user.getEmail());
		
		User updatedUser = userService.loginFromId(user.getId());
		
		assertEquals(user, updatedUser);
		
	}

	

}
