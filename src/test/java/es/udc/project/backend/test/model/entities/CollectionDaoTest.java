package es.udc.project.backend.test.model.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CollectionDaoTest {

	@Autowired
	private CollectionDao dao ;
	
	@Test
	public void findPermittedCollections() {
		List<Long> ids = new ArrayList<>();
		Collection col1=dao.save( new Collection(LocalDateTime.now(), "path", true, (long) 0));
		ids.add(col1.getId());
		Collection col2=dao.save( new Collection(LocalDateTime.now(), "path2", true, (long) 0));
		ids.add(col2.getId());
		List<Collection> expected= Arrays.asList(col1,col2);
		List<Collection> found=dao.findPermittedCollections(ids).getContent();
		
		assertEquals(expected,found);
	}
	
	@Test
	public void findByEnabled() {
		List<Long> ids = new ArrayList<>();
		Collection col1=dao.save( new Collection(LocalDateTime.now(), "path", true, (long) 0));
		ids.add(col1.getId());
		Collection col2=dao.save( new Collection(LocalDateTime.now(), "path2", true, (long) 0));
		ids.add(col2.getId());
		Collection col3=dao.save( new Collection(LocalDateTime.now(), "path2", false, (long) 0));
		List<Collection> expectedTrue= Arrays.asList(col1,col2);
		List<Collection> expectedFalse= Arrays.asList(col3);
		List<Collection> foundTrue=dao.findByEnabled(true).getContent();
		List<Collection> foundFalse=dao.findByEnabled(false).getContent();
		assertEquals(expectedTrue,foundTrue);
		assertEquals(expectedFalse,foundFalse);
		
	}
}
