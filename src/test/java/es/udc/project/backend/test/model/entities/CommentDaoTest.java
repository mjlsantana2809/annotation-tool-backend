package es.udc.project.backend.test.model.entities;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;
import es.udc.project.backend.model.entities.Comment;
import es.udc.project.backend.model.entities.CommentDao;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Psychological_Analysis.Pathology;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.UserDao;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;
import es.udc.project.backend.model.entities.User.RoleType;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CommentDaoTest {

	@Autowired
	private CommentDao commentDao;
	@Autowired
	private Psychological_AnalysisDao analysisDao;
	@Autowired
	private CollectionDao collectionDao;
	@Autowired
	private WritingDao writingDao;
	@Autowired
	private UserDao userDao;
	@Test
	public void findByAnalysisIdAndStartAndEndTest() {
		Collection col1=collectionDao.save( new Collection(LocalDateTime.now(), "path", true, (long) 0));
		Writing writing= writingDao.save(new Writing(col1, "title", LocalDateTime.now(), "info", "textr", "author"));
		User user = userDao.save(new User("username", "firstName", "lastName", "testannotationtoolapp@gmail.com",
				RoleType.PSYCHOLOGIST,LocalDateTime.now()));
	
		Psychological_Analysis analysis = analysisDao.save( new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "diagnosis", false, user, writing, Pathology.NONE));
		
		Comment expected = commentDao.save(new Comment((long) 1, (long) 33, "textcomment", analysis, true));
		Comment found= commentDao.findByAnalysisIdAndStartAndEnd(analysis.getId(), (long)1, (long)33).get();
		
		assertEquals(expected,found);
	}
	@Test
	public void deleteByAnalysisIdAndStartAndEndTest() {
		Collection col1=collectionDao.save( new Collection(LocalDateTime.now(), "path", true, (long) 0));
		Writing writing= writingDao.save(new Writing(col1, "title", LocalDateTime.now(), "info", "textr", "author"));
		User user = userDao.save(new User("username", "firstName", "lastName", "testannotationtoolapp@gmail.com",
				RoleType.PSYCHOLOGIST,LocalDateTime.now()));
	
		Psychological_Analysis analysis = analysisDao.save( new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "diagnosis", false, user, writing, Pathology.NONE));
		
		commentDao.save(new Comment((long) 1, (long) 33, "textcomment", analysis, true));
		Long deleted=commentDao.deleteByAnalysisIdAndStartAndEnd(analysis.getId(), (long)1, (long)33);
		
		assertEquals((long)1,deleted);
	}
	
	@Test
	public void findByAnalysisIdTest() {
		Collection col1=collectionDao.save( new Collection(LocalDateTime.now(), "path", true, (long) 0));
		Writing writing= writingDao.save(new Writing(col1, "title", LocalDateTime.now(), "info", "textr", "author"));
		User user = userDao.save(new User("username", "firstName", "lastName", "testannotationtoolapp@gmail.com",
				RoleType.PSYCHOLOGIST,LocalDateTime.now()));
	
		Psychological_Analysis analysis = analysisDao.save( new Psychological_Analysis(LocalDateTime.now(), LocalDateTime.now(), "diagnosis", false, user, writing, Pathology.NONE));
		
		Comment comment1=commentDao.save(new Comment((long) 1, (long) 33, "textcomment", analysis, true));
		Comment comment2=commentDao.save(new Comment((long) 1, (long) 33, "textcomment", analysis, true));
		
		List<Comment> found= commentDao.findByAnalysisId(analysis.getId());
		assertEquals(Arrays.asList(comment1,comment2), found);
	}
}
