package es.udc.project.backend.test.model.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Slice;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.services.ApiResponse;
import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.PermissionDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.EmptyCollectionException;
import es.udc.project.backend.model.exceptions.IncorrectPasswordException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.model.entities.UserDao;
import es.udc.project.backend.model.services.CollectionService;
import es.udc.project.backend.model.services.UserService;

@SpringBootTest
@ActiveProfiles("test")
@Transactional
public class CollectionServiceTest {
	
	private final Long NON_EXISTENT_ID = new Long(-1);
	
	@Autowired
	private CollectionService collectionService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CollectionDao collectionDao;
	
	@Autowired
	private WritingDao writingDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	@Autowired
	private UserDao userDao;
	
	private Collection newCollection(String name) {
		
		return new Collection(LocalDateTime.now(), "src/main/resources/"+name, true,(long)0);
	}
	
	private User createExpert(String userName, int cont) {
		Random rd = new Random();
		if(rd.nextBoolean()) {
			return new User(userName+cont, "firstName", "lastName", "testannotationtoolapp@gmail.com",
					RoleType.LINGUIST,LocalDateTime.now());
		}else {
			return new User(userName+cont, "firstName", "lastName", "testannotationtoolapp@gmail.com",
					RoleType.PSYCHOLOGIST,LocalDateTime.now());
		}
		
	}
	
	private User createAdmin(String userName) {
		
		return new User(userName, "firstName", "lastName", "testannotationtoolapp@gmail.com",
				RoleType.ADMIN,LocalDateTime.now());
	}
	
	private List<Collection> createMultipleCollectionsWithPosts() {
		Collection col1=collectionService.createCollection("collection1");
		Collection col2=collectionService.createCollection("collection2");
		Writing writing1 = new Writing(col1, "title1", LocalDateTime.now(), "info", "text1", "author1");
		Writing writing2 = new Writing(col1, "title2", LocalDateTime.now(), "info", "text2", "author1");
		Writing writing3 = new Writing(col2, "title3", LocalDateTime.now(), "info", "text3", "author1");
		Writing writing4 = new Writing(col1, "title4", LocalDateTime.now(), "info", "text4", "author2");
		
		writingDao.saveAll(Arrays.asList(writing1,writing2,writing3,writing4));
		writingDao.save(writing3);
		
		col1.setWritings(Arrays.asList(writing1,writing2,writing4));
		col2.setWritings(Arrays.asList(writing3));
		return (Arrays.asList(col1,col2));
	}
	
	
	@Test
	public void createCollectionTest() {
		Collection expected = newCollection("collection1");
		Collection created= collectionService.createCollection("collection1");
		assertEquals(expected.getPath(), created.getPath());
		assertEquals(expected.getWritings(), created.getWritings());
	}
	
	@Test
	public void findAllCollections() {
		Collection col1 =collectionService.createCollection("collection1");
		Collection col2 =collectionService.createCollection("collection2");
		Collection col3 =collectionService.createCollection("collection3");
		
		Block<Collection> expectedBlock = new Block<>(Arrays.asList(col1,col2,col3), false);
		assertEquals(expectedBlock, collectionService.findAllCollections(0, 10));
		assertEquals(new Block<>(Arrays.asList(col1,col2), true), collectionService.findAllCollections(0, 2));
	}
	
	@Test
	public void findPermittedCollections() throws DuplicatedInstanceException, InstanceNotFoundException {
		User user1 = createExpert("user1",1);
		User user2 = createExpert("user2",2);
		userService.signUp(user1, false);
		userService.signUp(user2, false);
		
		Collection col1= collectionService.createCollection("collection1");
		Collection col2= collectionService.createCollection("collection2");
		Collection col3= collectionService.createCollection("collection3");
		
		Permission permission1 = new Permission(user1, col1, LocalDateTime.now(), "ALL");
		Permission permission2 = new Permission(user1, col3, LocalDateTime.now(), "ALL");
		
		permissionDao.saveAll(Arrays.asList(permission1,permission2));
		
		user1.setPermissions(Arrays.asList(permission1,permission2));
		List<Collection> list =collectionService.findPermittedCollections(user1.getId());
		assertTrue(Arrays.asList(col3,col1).containsAll(list));
	}
	
	@Test
	public void findByCollectionIdAdmin() throws InstanceNotFoundException, PermissionException  {

		Collection col1= collectionService.createCollection("collection1");
		Collection col2= collectionService.createCollection("collection2");
		
		Writing writing1 = new Writing(col1, "title1", LocalDateTime.now(), "info", "text1", "author1");
		Writing writing2 = new Writing(col1, "title2", LocalDateTime.now(), "info", "text2", "author1");
		Writing writing3 = new Writing(col2, "title3", LocalDateTime.now(), "info", "text3", "author1");
		Writing writing4 = new Writing(col1, "title4", LocalDateTime.now(), "info", "text4", "author2");
		
		writingDao.saveAll(Arrays.asList(writing1,writing2,writing3,writing4));
		
		col1.setWritings(Arrays.asList(writing1,writing2,writing4));
		col2.setWritings(Arrays.asList(writing3));
		
		User user =userDao.save(createAdmin("admin"));

		assertEquals(new Block<>(col1.getWritings(), false),collectionService.findByCollectionId(user.getId(),col1.getId(), 0, 10));

	}
	
	@Test
	public void findByCollectionIdExpertWithNoPermission()  {
		Collection col1= collectionService.createCollection("collection1");
		Collection col2= collectionService.createCollection("collection2");
		
		Writing writing1 = new Writing(col1, "title1", LocalDateTime.now(), "info", "text1", "author1");
		Writing writing2 = new Writing(col1, "title2", LocalDateTime.now(), "info", "text2", "author1");
		Writing writing3 = new Writing(col2, "title3", LocalDateTime.now(), "info", "text3", "author1");
		Writing writing4 = new Writing(col1, "title4", LocalDateTime.now(), "info", "text4", "author2");
		
		writingDao.saveAll(Arrays.asList(writing1,writing2,writing3,writing4));
		
		col1.setWritings(Arrays.asList(writing1,writing2,writing4));
		col2.setWritings(Arrays.asList(writing3));
		
		User user =userDao.save(createExpert("expert",1));

		
		assertThrows(PermissionException.class, () ->
		collectionService.findByCollectionId(user.getId(),col1.getId(), 0, 10));
		
	}
	
	@Test
	public void findByCollectionIdExpertWithPermission() throws InstanceNotFoundException, PermissionException  {

		Collection col1= collectionService.createCollection("collection1");
		Collection col2= collectionService.createCollection("collection2");
		
		Writing writing1 = new Writing(col1, "title1", LocalDateTime.now(), "info", "text1", "author1");
		Writing writing2 = new Writing(col1, "title2", LocalDateTime.now(), "info", "text2", "author1");
		Writing writing3 = new Writing(col2, "title3", LocalDateTime.now(), "info", "text3", "author1");
		Writing writing4 = new Writing(col1, "title4", LocalDateTime.now(), "info", "text4", "author2");
		
		writingDao.saveAll(Arrays.asList(writing1,writing2,writing3,writing4));
		
		col1.setWritings(Arrays.asList(writing1,writing2,writing4));
		col2.setWritings(Arrays.asList(writing3));
		
		User user =userDao.save(createExpert("expert",1));
		
		permissionDao.save(new Permission(user, col2, LocalDateTime.now(), "ALL"));

		assertEquals(new Block<>(col2.getWritings(), false),collectionService.findByCollectionId(user.getId(),col2.getId(), 0, 10));

	}
	
	@Test
	public void updateCollectionStatus() throws InstanceNotFoundException {
		Collection col1= collectionService.createCollection("collection1");
		Collection updatedCollection=collectionService.updateCollectionStatus(false, col1.getId());
		
		assertEquals(false, updatedCollection.isEnabled());
		assertEquals(true,collectionService.updateCollectionStatus(true, col1.getId()).isEnabled() );
		
	}
	
	@Test
	public void updateCollectionStatusNonExistentCollectionId() throws InstanceNotFoundException {
		assertThrows(InstanceNotFoundException.class, () ->
		collectionService.updateCollectionStatus(false, NON_EXISTENT_ID));
		
	}
	
	@Test
	public void updatePermission() throws InstanceNotFoundException {
		Collection col1= collectionService.createCollection("collection1");
		User user =userDao.save(createAdmin("admin"));
		
		collectionService.updatePermission(user.getId(), col1.getId());
		
		assertTrue(permissionDao.findByCollectionIdAndUserId(col1.getId(), user.getId()).isPresent());
		
		collectionService.updatePermission(user.getId(), col1.getId());
		
		assertFalse(permissionDao.findByCollectionIdAndUserId(col1.getId(), user.getId()).isPresent());
	}
	
	@Test
	public void updatePermissionNonExistentCollection() throws InstanceNotFoundException {
		User user =userDao.save(createAdmin("admin"));
		
		assertThrows(InstanceNotFoundException.class, () ->
		collectionService.updatePermission(user.getId(),NON_EXISTENT_ID));
		

	}
	
	@Test
	public void updatePermissionNonExistentUser() throws InstanceNotFoundException {
		Collection col1= collectionService.createCollection("collection1");
		
		assertThrows(InstanceNotFoundException.class, () ->
		collectionService.updatePermission(NON_EXISTENT_ID,col1.getId()));
	}
	
	@Test
	public void importByKeywords() throws EmptyCollectionException {
		Collection collection = collectionService.importByKeywords("depression","", "collectionTest", 100);
		Collection expected = collectionDao.findById(collection.getId()).get();
		assertEquals(expected,collection);
	}
	
	@Test
	public void findPostByIdAsAdmin() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		Writing expected =collections.get(0).getWritings().get(0);
		User user =userDao.save(createAdmin("admin"));
		assertEquals(expected,collectionService.findPostById(expected.getId(), user.getId()));
		
	}
	@Test
	public void findPostByIdNonExistendId() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		Writing expected =collections.get(0).getWritings().get(0);
		User user =userDao.save(createAdmin("admin"));
		assertThrows(InstanceNotFoundException.class, () ->collectionService.findPostById(expected.getId(), NON_EXISTENT_ID));
		
	}
	
	@Test
	public void findPostByIdNonExistentPermission() {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		Writing expected =collections.get(0).getWritings().get(0);
		User user =userDao.save(createExpert("expert", 1));
		assertThrows(PermissionException.class, () ->collectionService.findPostById(expected.getId(), user.getId()));
	}
	
	@Test
	public void findPostByIdAsExpert() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		Writing expected =collections.get(0).getWritings().get(0);
		User user =userDao.save(createExpert("expert", 1));
		collectionService.updatePermission(user.getId(), collections.get(0).getId());
		assertEquals(expected,collectionService.findPostById(expected.getId(), user.getId()));
	}
	
	@Test
	public void findPostsByAuthorAsAdmin() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		User user =userDao.save(createAdmin("admin"));
		List <Writing> expectedList= new ArrayList<>();
		for(Collection collection:collections) {
			for(Writing writing: collection.getWritings()) {
				if(writing.getAuthor().equals("author1")) {
					expectedList.add(writing);
				}
			}
		}
		assertEquals(new Block<>(expectedList, false),collectionService.findPostsByAuthor(user.getId(), "author1", 0, 10));
	}
	
	@Test
	public void findPostsByAuthorNonExistentId() throws InstanceNotFoundException, PermissionException {
		User user =userDao.save(createAdmin("admin"));
		List<Writing> expectedList = new ArrayList<>();
		assertEquals(new Block<>(expectedList, false),collectionService.findPostsByAuthor(user.getId(), "NON_EXISTENT_AUTHOR", 0, 10));
	}
	
	@Test
	public void findPostsByAuthorAsExpertWithoutPermission() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		User user =userDao.save(createExpert("expert",1));
		List<Writing> expectedList = new ArrayList<>();
		Block<Writing> founded= collectionService.findPostsByAuthor(user.getId(), "author1", 0, 10);
		assertEquals(new Block<>(expectedList, false),collectionService.findPostsByAuthor(user.getId(), "author1", 0, 10));
	}
	@Test
	public void findPostsByAuthorAsExpert() throws InstanceNotFoundException, PermissionException {
		List<Collection> collections=createMultipleCollectionsWithPosts();
		User user =userDao.save(createExpert("expert",1));
		
		List <Writing> expectedList= new ArrayList<>();
		Permission permission=permissionDao.save(new Permission(user, collections.get(0), LocalDateTime.now(), "ALL"));
		user.setPermissions(Arrays.asList(permission));
		for(Writing writing: collections.get(0).getWritings()) {
			if(writing.getAuthor().equals("author1")) {
				expectedList.add(writing);
			}
		}
		Block<Writing> founded =collectionService.findPostsByAuthor(user.getId(), "author1", 0, 10);
		assertEquals(new Block<>(expectedList, false),founded);
	}
	
}
