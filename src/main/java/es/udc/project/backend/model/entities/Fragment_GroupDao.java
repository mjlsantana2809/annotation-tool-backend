package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface Fragment_GroupDao extends PagingAndSortingRepository<Fragment_Group, Long> {
	 void deleteByGroupIdAndFragmentId(String groupId, String fragmentId);
	 List<Fragment_Group> findByGroupAnalysisId(Long groupAnalysisId);
	 
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Group a  group by a.relation.name")
	 List<Object[]>countRelations();
	 
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Group a where a.fragment.analysis.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByAnalysis(Long analysisId);
	 Long countByFragmentAnalysisId(Long analysisId);
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Group a where a.fragment.analysis.writing.collection.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByCollection(Long collectionId);
	 Long countByFragmentAnalysisWritingCollectionId(Long collectionId);
}