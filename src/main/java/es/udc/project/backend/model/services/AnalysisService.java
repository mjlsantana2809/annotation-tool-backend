package es.udc.project.backend.model.services;

import java.util.List;

import es.udc.project.backend.model.entities.Analysis;
import es.udc.project.backend.model.entities.Comment;
import es.udc.project.backend.model.entities.Methodology;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Relation_Type;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadySharedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.CommentOutOfBoundsException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.InvalidInputException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.rest.dtos.params.CommentParamsDto;
import es.udc.project.backend.rest.dtos.params.PsychologicalAnalysisParamsDto;
import es.udc.project.backend.rest.dtos.params.SpeechAnalysisParamsDto;

public interface AnalysisService {

	public Long startAnalysis(Long writingId, Long userId) throws InstanceNotFoundException, PermissionException,AnalysisAlreadyStartedException;
	
	public Long startAnalysis(Long writingId, Long userId, Long methodologyId) throws InstanceNotFoundException, PermissionException,AnalysisAlreadyStartedException;
	
	public void finishAnalysis(Long analysisId, Long userId)  throws InstanceNotFoundException, PermissionException,AnalysisAlreadyFinishedException;
	
	public Psychological_Analysis continuePsychologicalAnalysis(Long postId, Long userId,Long targetUserId) throws InstanceNotFoundException, PermissionException,AnalysisAlreadyFinishedException;
	
	public SpeechAnalysisResult continueSpeechAnalysis(Long postId, Long userId,Long targetUserId)throws AnalysisAlreadyFinishedException, PermissionException;
	
	public void deleteComment(Long userId, Long analysisId,Long commentId)throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException ;
	
	public Psychological_Analysis saveAnalysis(Long userId, Long analysisId, PsychologicalAnalysisParamsDto analysisParams)throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException;

	public Speech_Analysis saveAnalysis(Long userId, Long analysisId, SpeechAnalysisParamsDto analysisParams)throws InstanceNotFoundException, AnalysisAlreadyFinishedException, PermissionException,DuplicatedInstanceException,InvalidInputException;
	
	public List<Methodology> findMethodologies();
	
	public List<Relation_Type> findRelations(Long methodologyId);
	
	public Block<Speech_Analysis> findSpeechAnalysisByUser(Long userId, boolean showFinished, int page, int size);
	
	public Block<Psychological_Analysis> findPsychologicalAnalysisByUser(Long userId, boolean showFinished, int page, int size);
	
	public Long shareAnalysis(Long userId, String targetUsername, Long analysisId)throws InstanceNotFoundException, PermissionException, AnalysisAlreadySharedException;
	
	public void unshareAnalysis(Long userId, Long analysisId, Long targetUserId)throws InstanceNotFoundException;
	
	public Block<Permission> findSharedAnalysisTo(Long targetUserId, int page, int size) throws InstanceNotFoundException;
	
	public Block<Permission> findMySharedAnalysis(Long userId, int page, int size) throws InstanceNotFoundException;
	
}
