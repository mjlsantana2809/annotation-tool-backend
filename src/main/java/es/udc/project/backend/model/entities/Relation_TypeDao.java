package es.udc.project.backend.model.entities;



import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface Relation_TypeDao extends PagingAndSortingRepository<Relation_Type, Long> {
	List<Relation_Type> findByMethodologyId(Long methodologyId);
}
