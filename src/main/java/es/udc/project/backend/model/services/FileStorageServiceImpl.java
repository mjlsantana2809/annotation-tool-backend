package es.udc.project.backend.model.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.exceptions.FileStorageException;
@Service
public class FileStorageServiceImpl implements FileStorageService {
	
	
	public String storeFile(MultipartFile file, String collectionName) throws FileStorageException {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = Paths.get("src/main/resources/collections/"+collectionName+"/"+fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            return targetLocation.toString();
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

	@Override
	public String createCollectionFolder(String collectionName) {
		File file = new File("src/main/resources/collections/"+collectionName);
		file.mkdir();
		return file.getPath();
	}
}
