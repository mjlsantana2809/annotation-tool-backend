package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MethodologyDao extends PagingAndSortingRepository<Methodology, Long> {

}