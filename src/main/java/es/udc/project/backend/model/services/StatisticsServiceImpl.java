package es.udc.project.backend.model.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Fragment_FragmentDao;
import es.udc.project.backend.model.entities.Fragment_GroupDao;
import es.udc.project.backend.model.entities.Group_GroupDao;
import es.udc.project.backend.model.entities.Psychological_Analysis.Pathology;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;



@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {

	@Autowired
	private Psychological_AnalysisDao psychologicalAnalysisDao;
	
	@Autowired
	private Fragment_FragmentDao fxfDao;
	
	@Autowired
	private Fragment_GroupDao fxgDao;
	
	@Autowired
	private Group_GroupDao gxgDao;
	
	
	@Override
	public Statistics countByPathology(Long collectionId) {
		List<Object[]> results = new ArrayList<>();
		long count=0;
		if(collectionId==null) {
			results= psychologicalAnalysisDao.countByPathology();
			count = psychologicalAnalysisDao.count();
		}else {
			results= psychologicalAnalysisDao.countByPathology(collectionId);
			count = psychologicalAnalysisDao.countByWritingCollectionId(collectionId);
		}
		
		
		return new Statistics(results, count);
		
		
	}

	@Override
	public Statistics countRelations() {
		List<Object[]> results = new ArrayList<>();
		long count=0;
			//Fragment_Fragment
			results.addAll( fxfDao.countRelations());
			count = count+ fxfDao.count();
			//Fragment_Group
			results.addAll( fxgDao.countRelations());
			count = count+ fxgDao.count();
			//Group_Group
			results.addAll( gxgDao.countRelations());
			count = count+ gxgDao.count();
		
	
		
		return new Statistics(results, count);
	}

	@Override
	public Statistics countRelationsByAnalysis(Long analysisId) {
		List<Object[]> results = new ArrayList<>();
		long count=0;
			//Fragment_Fragment
			results.addAll( fxfDao.countRelationsByAnalysis(analysisId));
			count = count+ fxfDao.countByFragmentFromAnalysisId(analysisId);
			//Fragment_Group
			results.addAll( fxgDao.countRelationsByAnalysis(analysisId));
			count = count+ fxgDao.countByFragmentAnalysisId(analysisId);
			//Group_Group
			results.addAll( gxgDao.countRelationsByAnalysis(analysisId));
			count = count+ gxgDao.countByGroupFromAnalysisId(analysisId);
		
	
		
		return new Statistics(results, count);
	}

	@Override
	public Statistics countRelationsByCollection(Long collectionId) {
		List<Object[]> results = new ArrayList<>();
		long count=0;
			//Fragment_Fragment
			results.addAll( fxfDao.countRelationsByCollection(collectionId));
			count = count+ fxfDao.countByFragmentFromAnalysisWritingCollectionId(collectionId);
			//Fragment_Group
			results.addAll( fxgDao.countRelationsByCollection(collectionId));
			count = count+ fxgDao.countByFragmentAnalysisWritingCollectionId(collectionId);
			//Group_Group
			results.addAll( gxgDao.countRelationsByCollection(collectionId));
			count = count+ gxgDao.countByGroupFromAnalysisWritingCollectionId(collectionId);
		
	
		
		return new Statistics(results, count);
	}
	


	
	

}
