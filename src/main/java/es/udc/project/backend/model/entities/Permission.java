package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Permission {
	private Long id;
	private User user;
	private Collection collection;
	private Psychological_Analysis psychologicalAnalysis;
	private Speech_Analysis speechAnalysis;
	private LocalDateTime date;
	private String type;
	
	public Permission() {
	}

	//Permisos de coleccion
	public Permission(User user, Collection collection, LocalDateTime date, String type) {
		this.user = user;
		this.collection = collection;
		this.date = date;
		this.type = type;
	}

	//Permisos de analisis psicologico
	public Permission(User user, Psychological_Analysis psychologicalAnalysis, LocalDateTime date,
			String type) {
		this.user = user;
		this.psychologicalAnalysis = psychologicalAnalysis;
		this.date = date;
		this.type = type;
	}

	//Permisos de analisis del discurso
	public Permission(User user, Speech_Analysis speechAnalysis, LocalDateTime date, String type) {
		this.user = user;
		this.speechAnalysis = speechAnalysis;
		this.date = date;
		this.type = type;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "collection")
	public Collection getCollection() {
		return collection;
	}
	
	public void setCollection(Collection collection) {
		this.collection = collection;
	}
	

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "psychologicalAnalysis")
	public Psychological_Analysis getPsychologicalAnalysis() {
		return psychologicalAnalysis;
	}


	public void setPsychologicalAnalysis(Psychological_Analysis psychologicalAnalysis) {
		this.psychologicalAnalysis = psychologicalAnalysis;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "speechAnalysis")
	public Speech_Analysis getSpeechAnalysis() {
		return speechAnalysis;
	}


	public void setSpeechAnalysis(Speech_Analysis speechAnalysis) {
		this.speechAnalysis = speechAnalysis;
	}


	public LocalDateTime getDate() {
		return date;
	}


	public void setDate(LocalDateTime date) {
		this.date = date;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
