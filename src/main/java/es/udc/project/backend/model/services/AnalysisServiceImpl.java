package es.udc.project.backend.model.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.Analysis;
import es.udc.project.backend.model.entities.Comment;
import es.udc.project.backend.model.entities.CommentDao;
import es.udc.project.backend.model.entities.Fragment;
import es.udc.project.backend.model.entities.FragmentDao;
import es.udc.project.backend.model.entities.Fragment_Fragment;
import es.udc.project.backend.model.entities.Fragment_FragmentDao;
import es.udc.project.backend.model.entities.Fragment_Group;
import es.udc.project.backend.model.entities.Fragment_GroupDao;
import es.udc.project.backend.model.entities.Group;
import es.udc.project.backend.model.entities.GroupDao;
import es.udc.project.backend.model.entities.Group_Group;
import es.udc.project.backend.model.entities.Group_GroupDao;
import es.udc.project.backend.model.entities.Methodology;
import es.udc.project.backend.model.entities.MethodologyDao;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.PermissionDao;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Psychological_Analysis.Pathology;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;
import es.udc.project.backend.model.entities.Relation_Type;
import es.udc.project.backend.model.entities.Relation_TypeDao;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.model.entities.Speech_AnalysisDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadySharedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.CommentOutOfBoundsException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.InvalidInputException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.rest.dtos.RelationDto;
import es.udc.project.backend.rest.dtos.params.CommentParamsDto;
import es.udc.project.backend.rest.dtos.params.GroupParamsDto;
import es.udc.project.backend.rest.dtos.params.PsychologicalAnalysisParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationTargetParamsDto;
import es.udc.project.backend.rest.dtos.params.SegmentParamsDto;
import es.udc.project.backend.rest.dtos.params.SpeechAnalysisParamsDto;
import net.bytebuddy.description.modifier.EnumerationState;

@Service
@Transactional
public class AnalysisServiceImpl implements AnalysisService{
	
	@Autowired
	private Psychological_AnalysisDao psychologicalAnalysisDao;
	
	@Autowired
	private Speech_AnalysisDao speechAnalysisDao;
	
	@Autowired
	private CollectionService collectionService;
	
	@Autowired
	private InternalService internalService;
	
	@Autowired
	private CommentDao commentDao;
	
	
	@Autowired
	private MethodologyDao methodologyDao;
	
	@Autowired
	private Relation_TypeDao relationDao;
	
	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private FragmentDao fragmentDao;
	
	@Autowired
	private Fragment_FragmentDao fXfDao;
	
	@Autowired
	private Fragment_GroupDao fXgDao;
	
	@Autowired
	private Group_GroupDao gXgDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	
	
	private Psychological_Analysis checkAnalysisAvailableAndYours(Long postId, Long userId,Long targetUserId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
		Optional<Psychological_Analysis> analysisOp;
		if(targetUserId!=null) {
			analysisOp= psychologicalAnalysisDao.findByUserIdAndWritingId(targetUserId, postId);
		}else {
			analysisOp= psychologicalAnalysisDao.findByUserIdAndWritingId(userId, postId);
		}
		
		if(!analysisOp.isPresent()) {
			return null;
		}
		
		Psychological_Analysis analysis = analysisOp.get();

		if(targetUserId!=null) {
			if(!permissionDao.findByPsychologicalAnalysisIdAndUserId(analysis.getId(), userId).isPresent()) {
				throw new PermissionException();
			}
		}
		return analysis;
	}

	@Override
	public Long startAnalysis(Long writingId, Long userId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		
		//Si el usuario no existe dará instance not found
		User user = internalService.checkUser(userId);
		
		//InstanceNotFound si el post no existe
		//PermissionException si el usuario no tiene los permisos necesarios			
		Writing writing = collectionService.findPostById(writingId, userId);
		if(psychologicalAnalysisDao.findByUserIdAndWritingId(userId, writingId).isPresent()) {
			throw new AnalysisAlreadyStartedException();
		}
		return  psychologicalAnalysisDao.save(new Psychological_Analysis( LocalDateTime.now(), LocalDateTime.now(), "", false, user, writing, Pathology.MORE_INFO_NEEDED)).getId();
	
	}
	
	@Override
	public Long startAnalysis(Long writingId, Long userId, Long methodologyId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException {
		
		//Si el usuario no existe dará instance not found
		User user = internalService.checkUser(userId);
		
		//InstanceNotFound si el post no existe
		//PermissionException si el usuario no tiene los permisos necesarios			
		Writing writing = collectionService.findPostById(writingId, userId);

		if(speechAnalysisDao.findByUserIdAndWritingId(userId, writingId).isPresent()) {
			throw new AnalysisAlreadyStartedException();
		}
		Optional<Methodology> methodology= methodologyDao.findById(methodologyId);
		if(!methodology.isPresent()) {
			throw new InstanceNotFoundException("project.entities.methodology", methodologyId);
		}
		return speechAnalysisDao.save(new Speech_Analysis(LocalDateTime.now(), LocalDateTime.now(), false, user, writing,methodology.get())).getId();
		
	
	}

	@Override
	public void finishAnalysis(Long analysisId, Long userId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
		if(internalService.isPsychologist(userId)) {
			Optional<Psychological_Analysis> analysisOp=psychologicalAnalysisDao.findById(analysisId);
			if(!analysisOp.isPresent()) {
				throw new InstanceNotFoundException("project.entities.psychologicalAnalysis", analysisId);
			}
			Psychological_Analysis analysis = analysisOp.get();
			if(analysis.isFinished()) {
				throw new AnalysisAlreadyFinishedException();
			}
			if(analysis.getUser().getId()!=userId) {
				throw new PermissionException();
			}
			analysis.setLastModification(LocalDateTime.now());
			analysis.setFinished(true);
		}else {
			 Optional<Speech_Analysis> analysisOp=speechAnalysisDao.findById(analysisId);
			 if(!analysisOp.isPresent()) {
					throw new InstanceNotFoundException("project.entities.speechAnalysis", analysisId);
				}
				Speech_Analysis analysis = analysisOp.get();
				if(analysis.isFinished()) {
					throw new AnalysisAlreadyFinishedException();
				}
				if(!analysis.getUser().getId().equals(userId)) {
					throw new PermissionException();
				}
				analysis.setLastModification(LocalDateTime.now());
				analysis.setFinished(true);
		}
		
		
		
	}

	@Override
	public Psychological_Analysis continuePsychologicalAnalysis(Long postId, Long userId,Long targetUserId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
		
		return checkAnalysisAvailableAndYours(postId, userId, targetUserId);
	}
	
	@Override
	public SpeechAnalysisResult continueSpeechAnalysis(Long postId, Long userId, Long targetUserId) throws AnalysisAlreadyFinishedException, PermissionException {
		Optional<Speech_Analysis> analysisOp;
		//Null = preguntamos por el propietario
		if(targetUserId==null) {
			analysisOp= speechAnalysisDao.findByUserIdAndWritingId(userId, postId);
		}else {
			analysisOp= speechAnalysisDao.findByUserIdAndWritingId(targetUserId, postId);
		}
		
		if(!analysisOp.isPresent()) {
			return null;
		}
		Speech_Analysis analysis = analysisOp.get();

		if(targetUserId!=null) {
			if(!permissionDao.findBySpeechAnalysisIdAndUserId(analysis.getId(), userId).isPresent()) {
				throw new PermissionException();
			}
		}
		
		List<Fragment> fragments = fragmentDao.findByAnalysisId(analysis.getId());
		List<Group> groups = groupDao.findByAnalysisId(analysis.getId());
		List<Fragment_Fragment> fxf = fXfDao.findByFragmentFromAnalysisId(analysis.getId());
		List<Fragment_Group> fxg = fXgDao.findByGroupAnalysisId(analysis.getId());
		List<Group_Group> gxg = gXgDao.findByGroupFromAnalysisId(analysis.getId());
		return new SpeechAnalysisResult(analysis, fragments, groups, fxf, fxg, gxg);
	}



	@Override
	public void deleteComment(Long userId, Long analysisId, Long commentId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
		checkAnalysisAvailableAndYours(analysisId, userId,null);
		if(!commentDao.findById(commentId).isPresent()) {
			throw new InstanceNotFoundException("project.entities.comment", commentId);
		}
		commentDao.deleteById(commentId);
	}

	@Override
	public Psychological_Analysis saveAnalysis(Long userId, Long analysisId,
			PsychologicalAnalysisParamsDto analysisParams) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException {
		
		Optional<Psychological_Analysis> analysisOp=psychologicalAnalysisDao.findById(analysisId);
		if(!analysisOp.isPresent()) {
			throw new InstanceNotFoundException("project.entities.psychologicalAnalysis", analysisId);
		}
		
		Psychological_Analysis analysis=checkAnalysisAvailableAndYours(analysisOp.get().getWriting().getId(), userId,null);
		Optional<Comment> commentOp;
		
		for(CommentParamsDto commentDto: analysisParams.getComments()) {
			commentOp=commentDao.findByAnalysisIdAndStartAndEnd(analysisId, commentDto.getStart(), commentDto.getEnd());
			switch(commentDto.getStatus()) {
				case 0:
					//Deberia estar en persistencia, pero no esta
					if(!commentOp.isPresent()) {
						throw new InstanceNotFoundException("project.entities.comment", -1);
					}
					break;
					
				case 1:		
					checkCommentBounds(analysis,commentDto);
					commentDao.save(new Comment(commentDto.getStart(), commentDto.getEnd(), commentDto.getText(), analysis, commentDto.isTitle()));
					break;
					
				case 2:
					commentDao.deleteByAnalysisIdAndStartAndEnd(analysisId, commentDto.getStart(), commentDto.getEnd());
					break;
					
				default:
					break;
					
			}
		}
		analysis.setComments(commentDao.findByAnalysisId(analysisId));
		analysis.setDiagnosis(analysisParams.getDiagnosis());
		analysis.setLastModification(LocalDateTime.now());
		analysis.setPathology(Pathology.values()[analysisParams.getPathology()]);
		
		return psychologicalAnalysisDao.save(analysis);
	}
	
	private void checkCommentBounds(Psychological_Analysis analysis, CommentParamsDto commentDto) throws CommentOutOfBoundsException {
			
		//Start < que end
		//Start +
		//End < longitud del post
		if(commentDto.isTitle()) {
			if(commentDto.getStart()>commentDto.getEnd() || commentDto.getStart()<0 || commentDto.getEnd()>analysis.getWriting().getTitle().length() -1) {
				throw new CommentOutOfBoundsException();
			}
		}else {
			if(commentDto.getStart()>commentDto.getEnd() || commentDto.getStart()<0 || commentDto.getEnd()>analysis.getWriting().getText().length() -1) {
				throw new CommentOutOfBoundsException();
			}
		}
		
		//Evitar superposicion de comentarios
		for (Comment comment: analysis.getComments()) {
			if(comment.isTitle()==commentDto.isTitle()) {
				if((comment.getStart()>=commentDto.getStart()&& comment.getStart()<=commentDto.getEnd())||
	                    (comment.getEnd()>=commentDto.getStart()&& comment.getEnd()<=commentDto.getEnd())||
	                    (commentDto.getStart()>=comment.getStart() && commentDto.getEnd() <= comment.getEnd())){
	                        throw new CommentOutOfBoundsException();
	            }
			}
			
		}
		
	}

	@Override
	public List<Methodology> findMethodologies() {
		List<Methodology> result = new ArrayList<>();
		Iterable<Methodology> methodologies=methodologyDao.findAll();
		methodologies.forEach(result::add);
		return result;
	}

	@Override
	public List<Relation_Type> findRelations(Long methodologyId) {
			
		return relationDao.findByMethodologyId(methodologyId);
	}

	@Override
	public Speech_Analysis saveAnalysis(Long userId, Long analysisId, SpeechAnalysisParamsDto analysisParams) throws InstanceNotFoundException, AnalysisAlreadyFinishedException, PermissionException, DuplicatedInstanceException, InvalidInputException {
		
		
		Optional<Speech_Analysis> analysisOp=speechAnalysisDao.findById(analysisId);
		List<RelationParamsDto> relations = analysisParams.getRelations();
		if(!analysisOp.isPresent()) {
			throw new InstanceNotFoundException("project.entities.psychologicalAnalysis", analysisId);
		}
		Speech_Analysis analysis = analysisOp.get();

		if(analysis.isFinished()) {
			throw new AnalysisAlreadyFinishedException();
		}
		
	
		
		for(GroupParamsDto groupP :analysisParams.getGroups()) {
			Group group = null;
			
			switch(groupP.getStatus()) {
				case 0:
					//el grupo está en persistencia
					Optional<Group> groupOp= groupDao.findById(groupP.getId());
					if(!groupOp.isPresent()) {
						throw new InstanceNotFoundException("project.entities.group", groupP.getId());
					}
					Group groupAux = groupOp.get();
					groupAux.setGroupLabel(groupP.getGroupLabel());
					groupAux.setPosX(groupP.getPosX());
					groupAux.setPosY(groupP.getPosY());
					if(groupP.getGroupId()!=null) {
						Optional<Group> targetGroupOp= groupDao.findById(groupP.getGroupId());
						if(!targetGroupOp.isPresent()) {
							throw new InstanceNotFoundException("project.entities.group", groupP.getGroupId());
						}
						groupAux.setGroup(targetGroupOp.get());
					}else {
						groupAux.setGroup(null);
					}
					group = groupDao.save(groupAux);
					break;
				case 1:
					//El grupo no está guardado en persistencia
					if(groupP.getGroupId()!=null) {
						Optional<Group> targetGroupOp= groupDao.findById(groupP.getGroupId());
						if(!targetGroupOp.isPresent()) {
							throw new InstanceNotFoundException("project.entities.group", groupP.getGroupId());
						}
						group= groupDao.save(new Group(groupP.getFeId(),analysis, groupP.getGroupLabel(), groupP.getPosX(), groupP.getPosY(), targetGroupOp.get()));
					}else{
						group=groupDao.save(new Group(groupP.getFeId(),analysis, groupP.getGroupLabel(), groupP.getPosX(), groupP.getPosY()));
					}
					
					updateRelationIds(relations, group.getId(), groupP.getFeId());
					break;
				case 2:
					for(RelationParamsDto relation:relations) {
						if((relation.getFrom().getType().equals("group") && relation.getFrom().getId() ==groupP.getId())||
							(relation.getTo().getType().equals("group") && relation.getTo().getId() ==groupP.getId())) {
						
								deleteRelation(relation.getFrom(),relation.getTo() ,analysis.getId() );
						}
					}
					for(SegmentParamsDto segmentP: groupP.getFragments()) {
						
						Fragment f =fragmentDao.findById(segmentP.getFeId()).get();
						f.setGroup(null);
						fragmentDao.save(f);
					}
					
					
					groupDao.deleteById(groupP.getId());
					break;
				default:
					throw new InvalidInputException();
			}
		
			
			for(SegmentParamsDto segmentP: groupP.getFragments()) {
				saveFragment(segmentP,analysis,group, relations);
			}
		}
		
		for(SegmentParamsDto segmentP: analysisParams.getSegments()) {
			saveFragment(segmentP,analysis,null, relations);
		}
		
		//REPETIR PROCEDIMIENTO PARA RELACIONES Y REALIZAR VALIDACIONES
		for(RelationParamsDto relationP: relations) {
			//Status 0 (persistente) -> Nada
			//Status 1 (nuevo) -> Se valida y se agrega
			//Status 2 (borrar) -> se borra
			
			switch (relationP.getStatus()) {
				case 0:
					break;
				case 1:
					saveRelation(relationP.getFrom(),relationP.getTo(), analysis.getId(), relationP.getRelation().getId());
					break;
				case 2:
					deleteRelation(relationP.getFrom(),relationP.getTo(), analysis.getId());
					break;
				default:
					throw new InvalidInputException();
			}
		}
		
		
		analysis.setLastModification(LocalDateTime.now());
		return analysis;
	}
	
	private void updateRelationIds(List<RelationParamsDto> relations, String id, String feId) {
		for(RelationParamsDto relation: relations) {
			
			
			if(relation.getFrom().getId().equals(feId)) {
				relation.getFrom().setId(id.toString());
			}else if(relation.getTo().getId().equals(feId)) {
				relation.getTo().setId(id.toString());
			}
			
			
		}
	}
	
	
	
	private void saveFragment(SegmentParamsDto segmentP, Speech_Analysis analysis, Group group,List<RelationParamsDto> relations ) throws DuplicatedInstanceException, InvalidInputException, InstanceNotFoundException {
		Optional<Fragment> fragmentOp=  fragmentDao.findByAnalysisIdAndStartAndEndAndTitle(analysis.getId(), segmentP.getStart(), segmentP.getEnd(),segmentP.isTitle());
		if(fragmentOp.isPresent()) {
			//Status 0 (persistente) -> Actualizamos
			//Status 1 (nuevo) -> Error
			//Status 2 (borrar) -> se borra
			switch (segmentP.getStatus()) {
				case 0:
					
						Fragment fragment = fragmentOp.get();
						fragment.setBackgroundColor(segmentP.getBgColor());
						fragment.setEnd(segmentP.getEnd());
						fragment.setFontColor(segmentP.getFontColor());
						fragment.setGroup(group);
						fragment.setPosX(segmentP.getPosX());
						fragment.setPosY(segmentP.getPosY());
						fragment.setStart(segmentP.getStart());
						fragmentDao.save(fragment);
			
					break;
				
				case 1:
					throw new DuplicatedInstanceException("project.entities.fragment",segmentP.getFeId() );
					
				case 2:
					Fragment f= fragmentOp.get();
					
					for(RelationParamsDto relation:relations) {
						if((relation.getFrom().getType().equals("segment") &&  relation.getFrom().getId() ==f.getId())||
							(relation.getTo().getType().equals("segment") &&  relation.getTo().getId() ==f.getId())) {
						
								deleteRelation(relation.getFrom(),relation.getTo() ,analysis.getId() );
						}
					}
					fragmentDao.delete(f);
					break;
					
				default:
					throw new InvalidInputException();
			}

		}else {
			//Si NO esta presente creamos uno nuevo
			//Status 0 (persistente) -> error
			//Status 1 (nuevo) -> se crea
			//Status 2 (borrar) -> error
			switch (segmentP.getStatus()) {
				case 0:
					throw new InstanceNotFoundException("project.entities.fragment",segmentP.getFeId());
					
				case 1:
					Fragment f=fragmentDao.save(new Fragment(segmentP.getFeId(),segmentP.getStart(), segmentP.getEnd(), segmentP.getFontColor(),
										segmentP.getBgColor(), analysis, group, segmentP.getPosX(), segmentP.getPosY(),segmentP.isTitle()));
					updateRelationIds(relations, f.getId().toString(),segmentP.getFeId());
					break;
					
				case 2:
					throw new InvalidInputException();
					
				default:
					throw new InvalidInputException();
			}
			
		}
	}
	
	private void deleteRelation(RelationTargetParamsDto from,RelationTargetParamsDto to ,Long analysisId ) throws InstanceNotFoundException {

		if(from.getType().equals("group") && to.getType().equals("group")) {
			gXgDao.deleteByGroupFromIdAndGroupToId(from.getId() , to.getId());
			
		}else if(from.getType().equals("segment") && to.getType().equals("segment")) {
			
			Optional<Fragment> fragmentFromOp=fragmentDao.findById(from.getId());
			Optional<Fragment> fragmentToOp=fragmentDao.findById( to.getId() );
			
			if(fragmentFromOp.isPresent() && fragmentToOp.isPresent()) {
				fXfDao.deleteByFragmentFromIdAndFragmentToId(fragmentFromOp.get().getId(), fragmentToOp.get().getId());
			}
			
			
			
		}else if(from.getType().equals("group") && to.getType().equals("segment") ) {
			
			Optional<Fragment> fragmentToOp=fragmentDao.findById( to.getId() );
			if(fragmentToOp.isPresent()) {
				fXgDao.deleteByGroupIdAndFragmentId(from.getId(),to.getId());
			}
			
			
		}else if(from.getType().equals("segment") && to.getType().equals("group")) {
			
			Optional<Fragment> fragmentFromOp=fragmentDao.findById(from.getId());
			if(fragmentFromOp.isPresent()) {
				fXgDao.deleteByGroupIdAndFragmentId( to.getId(),from.getId());
			}
			
		}
	}
	
	private void saveRelation(RelationTargetParamsDto from,RelationTargetParamsDto to, Long analysisId, Long relationId) throws InstanceNotFoundException {
		
		Optional<Relation_Type> relationOp = relationDao.findById(relationId);
		if(!relationOp.isPresent()) {
			throw new InstanceNotFoundException("project.entities.relation", relationId);
		}
		
		//Group to group
		if(from.getType().equals("group") && to.getType().equals("group")) {
			
				
			
			Optional<Group>groupFrom= groupDao.findById(from.getId());
		
			Optional<Group>groupTo= groupDao.findById(to.getId());
			if(!groupTo.isPresent()) {
				throw new InstanceNotFoundException("project.entities.group",to.getId());
			}
			
			gXgDao.save( new Group_Group(groupFrom.get(), groupTo.get(), relationOp.get()) );
			
			
			
			
			//fragment to fragment
		}else if(from.getType().equals("segment") && to.getType().equals("segment")) {
		
		
			
			Optional<Fragment> fragmentFrom = fragmentDao.findById(from.getId());
			if(!fragmentFrom.isPresent()) {
		
				throw new InstanceNotFoundException("project.entities.fragment",from.getId());
			}
			Optional<Fragment> fragmentTo = fragmentDao.findById(to.getId());
			if(!fragmentTo.isPresent()) {

				throw new InstanceNotFoundException("project.entities.fragment",to.getId());
			}
			
			fXfDao.save( new Fragment_Fragment(fragmentFrom.get(), fragmentTo.get(), relationOp.get()) );
			
		
		//group to fragment
		}else if(from.getType().equals("group") && to.getType().equals("segment") ) {
		
			
			Optional<Group>groupFrom= groupDao.findById(from.getId());
			if(!groupFrom.isPresent()) {
				throw new InstanceNotFoundException("project.entities.group",from.getId());
			}
			Optional<Fragment> fragmentTo = fragmentDao.findById(to.getId());
			if(!fragmentTo.isPresent()) {
				throw new InstanceNotFoundException("project.entities.fragment",to.getId());
			}
			fXgDao.save( new Fragment_Group(fragmentTo.get(), groupFrom.get(), false, relationOp.get()));
			
		
		//fragment to group
		}else if(from.getType().equals("segment") && to.getType().equals("group")) {
		
			
			Optional<Fragment> fragmentFrom = fragmentDao.findById(from.getId());
			if(!fragmentFrom.isPresent()) {
				throw new InstanceNotFoundException("project.entities.fragment",from.getId());
			}
			Optional<Group>groupTo= groupDao.findById(to.getId());
			if(!groupTo.isPresent()) {
				throw new InstanceNotFoundException("project.entities.group",to.getId());
			}
			fXgDao.save( new Fragment_Group(fragmentFrom.get(), groupTo.get(), true, relationOp.get()) );
			
		}
	}

	@Override
	public Block<Speech_Analysis> findSpeechAnalysisByUser(Long userId, boolean showFinished, int page, int size) {	
		Slice<Speech_Analysis> result;
		if(showFinished) {
			result= speechAnalysisDao.findByUserIdOrderByLastModificationDesc(userId, PageRequest.of(page, size));
			return new Block<>(result.getContent(),result.hasNext());
		}else {
			
			return new Block<>(speechAnalysisDao.findByUserIdAndFinishedOrderByLastModificationDesc(userId, showFinished),false);
		}
		
	}

	@Override
	public Block<Psychological_Analysis> findPsychologicalAnalysisByUser(Long userId, boolean showFinished, int page, int size) {
		Slice<Psychological_Analysis> result;
		if(showFinished) {
			result= psychologicalAnalysisDao.findByUserIdOrderByLastModificationDesc(userId, PageRequest.of(page, size));
			return new Block<>(result.getContent(),result.hasNext());
		}else {
			return new Block<>(psychologicalAnalysisDao.findByUserIdAndFinishedOrderByLastModificationDesc(userId, showFinished),false);
			
		}
		
	}

	@Override
	public Long shareAnalysis(Long userId, String targetUsername, Long analysisId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadySharedException {
		User user=internalService.checkUser(userId);
		User targetUser=internalService.checkUser(targetUsername);
		if(user.getRole()!=targetUser.getRole()) {
			throw new PermissionException();
		}
		if(user.getRole().name()=="PSYCHOLOGIST") {
			
			if(permissionDao.findByPsychologicalAnalysisIdAndUserId(analysisId, targetUser.getId()).isPresent()) {
				throw new AnalysisAlreadySharedException();
			}
			
			Optional<Psychological_Analysis> analysis= psychologicalAnalysisDao.findById(analysisId);
			if(!analysis.isPresent()) {
				throw new InstanceNotFoundException("project.entities.psychologicalAnalysis", analysisId);
			}
			return permissionDao.save(new Permission(targetUser, analysis.get(), LocalDateTime.now(), "READ")).getId() ;
		}else {
			
			if(permissionDao.findBySpeechAnalysisIdAndUserId(analysisId, targetUser.getId()).isPresent()) {
				throw new AnalysisAlreadySharedException();
			}
			
			Optional<Speech_Analysis> analysis= speechAnalysisDao.findById(analysisId);
			if(!analysis.isPresent()) {
				throw new InstanceNotFoundException("project.entities.speechAnalysis", analysisId);
			}
			return permissionDao.save(new Permission(targetUser, analysis.get(), LocalDateTime.now(), "READ")).getId() ;
		}
	}

	@Override
	public void unshareAnalysis(Long userId, Long analysisId, Long targetUserId) throws InstanceNotFoundException {
		User user=internalService.checkUser(userId);
		internalService.checkUser(targetUserId);
		Optional<Permission> permission;
		if(user.getRole().name()=="PSYCHOLOGIST") {
			permission= permissionDao.findByPsychologicalAnalysisIdAndUserId(analysisId, targetUserId);
			
		}else {
			permission= permissionDao.findBySpeechAnalysisIdAndUserId(analysisId, targetUserId);
		}
		if(!permission.isPresent()) {
			throw new InstanceNotFoundException("project.entities.psychologicalAnalysis", analysisId);
		}
		permissionDao.delete(permission.get());
	}

	@Override
	public Block<Permission> findSharedAnalysisTo(Long targetUserId, int page, int size) throws InstanceNotFoundException {
		internalService.checkUser(targetUserId);
		Slice<Permission> list= permissionDao.findByUserIdAndType(targetUserId, "READ",PageRequest.of(page, size));
		
		return new Block<>(list.getContent(),list.hasNext());
	}

	@Override
	public Block<Permission> findMySharedAnalysis(Long userId, int page, int size) throws InstanceNotFoundException {
		User user=internalService.checkUser(userId);
		Slice<Permission> list;
		if(user.getRole().name()=="PSYCHOLOGIST") {
			list= permissionDao.findByPsychologicalAnalysisUserIdAndType(userId, "READ",PageRequest.of(page, size));
		}else {
			list= permissionDao.findBySpeechAnalysisUserIdAndType(userId, "READ",PageRequest.of(page, size));
		}
		return new Block<>(list.getContent(),list.hasNext());
	}
	
	

	
	

}
