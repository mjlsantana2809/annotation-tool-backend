package es.udc.project.backend.model.entities;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface UserDao extends PagingAndSortingRepository<User, Long> {
	
	boolean existsByUserName(String userName);

	Optional<User> findByUserName(String userName);
	
	
	@Query("SELECT u FROM User u WHERE u.role != 0 ORDER BY u.userName ASC")
	Slice<User> findByRoleNot(int role,Pageable pageable);
	
}
