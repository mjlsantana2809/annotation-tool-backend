package es.udc.project.backend.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Fragment {
	private String id;
	private Long start;
	private Long end;
	private String fontColor;
	private String BackgroundColor;
	private Speech_Analysis analysis;
	@Column(name = "FGroup", nullable = true)
	private Group group;
	private Long posX;
	private Long posY;
	private boolean title;
	
	public Fragment() {
	}
	
	public Fragment(String id,Long start, Long end, String fontColor, String backgroundColor, Speech_Analysis analysis,
			Group group, Long posX, Long posY, boolean title) {
		this.id=id;
		this.start = start;
		this.end = end;
		this.fontColor = fontColor;
		BackgroundColor = backgroundColor;
		this.analysis = analysis;
		this.group = group;
		this.posX = posX;
		this.posY = posY;
		this.title=title;
	}
	
	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public String getBackgroundColor() {
		return BackgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		BackgroundColor = backgroundColor;
	}
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "analysis")
	public Speech_Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Speech_Analysis analysis) {
		this.analysis = analysis;
	}
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "fGroup")
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Long getPosX() {
		return posX;
	}

	public void setPosX(Long posX) {
		this.posX = posX;
	}

	public Long getPosY() {
		return posY;
	}

	public void setPosY(Long posY) {
		this.posY = posY;
	}

	public boolean isTitle() {
		return title;
	}

	public void setTitle(boolean title) {
		this.title = title;
	}
	

}
