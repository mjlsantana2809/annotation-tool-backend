package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface WritingDao extends PagingAndSortingRepository<Writing, Long> {
	Slice<Writing> findByAuthor(String author,Pageable pageable);
	
	Slice<Writing> findByCollectionId(Long collectionId, Pageable pageable);
	
	Slice<Writing> findByCollectionIdInAndAuthor(List<Long> collection,String Author, Pageable pageable);
}