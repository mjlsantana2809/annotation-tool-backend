package es.udc.project.backend.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Comment {

    private Long id;
	private Long start;
	private Long end;
	private String text;
	private Psychological_Analysis analysis;
	private boolean title;
	
	public Comment() {
	}
	
	public Comment(Long start, Long end, String text,
			Psychological_Analysis analysis, boolean title) {
		this.start = start;
		this.end = end;
		this.text = text;
		this.analysis = analysis;
		this.title= title;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "analysis")
	public Psychological_Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Psychological_Analysis analysis) {
		this.analysis = analysis;
	}

	public boolean isTitle() {
		return title;
	}

	public void setTitle(boolean title) {
		this.title = title;
	}
	
	
	
	
	
}
