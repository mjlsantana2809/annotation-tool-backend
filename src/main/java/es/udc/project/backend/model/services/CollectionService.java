package es.udc.project.backend.model.services;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.Individual;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.exceptions.EmptyCollectionException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;



public interface CollectionService {
	public void  toJavaObject(String path, Collection collection) throws IOException, NoSuchFileException;

	public void parseAllFiles(String path) throws IOException, NoSuchFileException;
	
	public Collection createCollection(String collectionName);
	
	public Block<Collection> findAllCollections( int page, int size);
	
	public List<Collection> findPermittedCollections( Long userId)throws InstanceNotFoundException;
	
	public Block<Writing> findByCollectionId(Long userId,Long collectionId, int page, int size)  throws InstanceNotFoundException, PermissionException;
	
	public Collection updateCollectionStatus(boolean enabled, Long collectionId) throws InstanceNotFoundException;
	
	public void updatePermission(Long userId, Long collectionId) throws InstanceNotFoundException;
	
	public Collection importByKeywords(String subrredit, String keywords,String collectionName, int size) throws EmptyCollectionException ;
	
	public Writing findPostById(Long postId, Long userId) throws PermissionException, InstanceNotFoundException;
	
	public Block<Writing> findPostsByAuthor(Long userId, String author,int page, int size) throws PermissionException, InstanceNotFoundException;
}
