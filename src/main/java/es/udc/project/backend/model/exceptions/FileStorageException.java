package es.udc.project.backend.model.exceptions;
@SuppressWarnings("serial")
public class FileStorageException extends RuntimeException {

	public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}