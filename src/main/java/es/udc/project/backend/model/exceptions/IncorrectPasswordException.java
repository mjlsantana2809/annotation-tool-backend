package es.udc.project.backend.model.exceptions;

@SuppressWarnings("serial")
public class IncorrectPasswordException extends Exception {}
