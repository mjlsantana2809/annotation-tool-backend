package es.udc.project.backend.model.services;

import java.util.List;

public class ApiResponse {

	
	private List<Post> data;
	
	
	public ApiResponse() {
	}
	
	public ApiResponse(List<Post> data) {
		this.data = data;
	}

	public List<Post> getData() {
		return data;
	}

	public void setData(List<Post> data) {
		this.data = data;
	}
	
	
}
