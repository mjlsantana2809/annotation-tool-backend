package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.BatchSize;
@Entity
@XmlRootElement(name = "WRITING")
public class Writing {

	private Long id;
	private Collection collection;
	private String title;
	private LocalDateTime date;
	private String info;
	private String text;
	private String author;
	private String xmlDate;
	private int status;
	
	
	public Writing() {
	}

	public Writing(Collection collection, String title, LocalDateTime date, String info, String text, String author) {
		this.collection = collection;
		this.title = title;
		this.date = date;
		this.info = info;
		this.text = text;
		this.author = author;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "collection")
	public Collection getCollection() {
		return collection;
	}

	
	public void setCollection(Collection collection) {
		this.collection = collection;
	}

	@XmlElement(name = "TITLE")
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public LocalDateTime getDate() {
		return date;
	}


	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@XmlElement(name = "INFO")
	public String getInfo() {
		return info;
	}


	public void setInfo(String info) {
		this.info = info;
	}

	@XmlElement(name = "TEXT")
	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}
	@Transient
	@XmlElement(name = "DATE")
	public String getXmlDate() {
		return xmlDate;
	}

	public void setXmlDate(String xmlDate) {
		this.xmlDate = xmlDate.trim();
		this.date = LocalDateTime.parse(xmlDate.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	
	@Transient
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
