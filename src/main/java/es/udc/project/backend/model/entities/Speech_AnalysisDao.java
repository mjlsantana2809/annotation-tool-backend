package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface Speech_AnalysisDao extends PagingAndSortingRepository<Speech_Analysis, Long> {
	Optional<Speech_Analysis> findByUserIdAndWritingId(Long userId, Long writingId);
	List<Speech_Analysis> findByUserIdAndFinishedOrderByLastModificationDesc(Long userId, boolean finished);
	Slice<Speech_Analysis> findByUserIdOrderByLastModificationDesc(Long userId, Pageable pageable);
}