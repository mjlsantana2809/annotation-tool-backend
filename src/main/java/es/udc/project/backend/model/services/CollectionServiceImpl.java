package es.udc.project.backend.model.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;
import es.udc.project.backend.model.entities.Individual;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.PermissionDao;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Psychological_AnalysisDao;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.model.entities.Speech_AnalysisDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.entities.WritingDao;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.EmptyCollectionException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import reactor.core.publisher.Mono;

@Service
@Transactional
public class CollectionServiceImpl implements CollectionService {
	
	@Autowired
	private WritingDao writingDao;
	@Autowired
	private CollectionDao collectionDao;
	
	@Autowired
	private Psychological_AnalysisDao psychologicalAnalysisDao;
	
	@Autowired
	private Speech_AnalysisDao speechAnalysisDao;
	
	@Autowired
	private InternalService internalService;
	
	@Autowired
	private PermissionDao permissionDao;
	
	private static final int MAX_API=100;
	
	@Override
	@Async("asyncExecutor")
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public void toJavaObject(String path, Collection collection) throws IOException, NoSuchFileException {
		try {

			File file = new File(path);
			JAXBContext jaxbContext = JAXBContext.newInstance(Individual.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Individual individual = (Individual) jaxbUnmarshaller.unmarshal(file);

			Iterable<Writing> writingIt = individual.getWritings();
			List<Writing> writingList = new ArrayList<>();
			writingIt.forEach(l -> writingList.add(l));
			for (Writing writing : writingList) {
				writing.setAuthor(individual.getAuthor());
				writing.setCollection(collection);
			}
			writingDao.saveAll(writingList);	
			collection.setPosts(collection.getPosts()+(long)writingList.size());
			collectionDao.save(collection);
            
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void parseAllFiles(String path) throws IOException, NoSuchFileException {

		try (Stream<Path> filesPath = Files.walk(Paths.get(path))) {

			List<String> resultList = (List<String>) filesPath.filter(Files::isRegularFile).map(x -> x.toString())
					.collect(Collectors.toList());
			Collection collection =collectionDao.save(new Collection(LocalDateTime.now(),path, true,(long)0));
			for (String result : resultList) {
				toJavaObject(result,collection);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Collection createCollection(String collectionName) {
		return collectionDao.save(new Collection(LocalDateTime.now(),"src/main/resources/"+collectionName, true,(long)0));
	
	}

	@Override
	@Transactional(readOnly = true)
	public Block<Collection> findAllCollections(int page, int size) {
		Slice<Collection> collections = collectionDao.findAll(PageRequest.of(page, size));
		return new Block<>(collections.getContent(), collections.hasNext());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Collection> findPermittedCollections(Long userId) throws InstanceNotFoundException {
		User user = internalService.checkUser(userId);
		
		List<Permission> permissions = permissionDao.findByUserIdAndCollectionNotNull(userId);
		Set<Collection> collections = new HashSet<>();
		if(permissions!=null) {
			permissions.forEach(permission ->{
					if(permission.getCollection().isEnabled()) {
						collections.add(permission.getCollection());
					}
				});
		}
		
		//Slice<Collection> collections = collectionDao.findPermittedCollections(collectionsId);

		return new ArrayList<Collection>(collections);
	}

	@Override
	@Transactional(readOnly = true)
	public Block<Writing> findByCollectionId(Long userId, Long collectionId, int page, int size) throws InstanceNotFoundException, PermissionException {
		if(!permissionDao.findByCollectionIdAndUserId(collectionId, userId).isPresent() && !internalService.isAdmin(userId))
			throw new PermissionException();
		Slice<Writing> writingsSlice= writingDao.findByCollectionId(collectionId,PageRequest.of(page, size));
		List<Writing> writings = writingsSlice.getContent();
		
		
		
		return new Block<>(prepareStatus(writings, userId),writingsSlice.hasNext());
	}

	@Override
	public Collection updateCollectionStatus(boolean enabled, Long collectionId) throws InstanceNotFoundException {
		Optional<Collection> collectionOp=collectionDao.findById(collectionId);
		if (!collectionOp.isPresent()) {
			throw new InstanceNotFoundException("project.entities.collection", collectionId);
		}
		Collection collection = collectionOp.get();
		collection.setEnabled(enabled);
		return collection;
	}

	@Override
	public void updatePermission(Long userId, Long collectionId) throws InstanceNotFoundException {
		User user = internalService.checkUser(userId);
		Collection collection = internalService.checkCollection(collectionId);
		
		Optional<Permission> permissionOp=permissionDao.findByCollectionIdAndUserId(collectionId, userId);
		if(permissionOp.isPresent()) {
			permissionDao.delete(permissionOp.get());
		}else {
			Permission permission = new Permission(user, collection, LocalDateTime.now(), "ALL");
			permissionDao.save(permission); 
		}
		
		
	}

	@Override
	public Collection importByKeywords(String subreddit, String keywords, String collectionName, int size) throws EmptyCollectionException {
		WebClient client = WebClient
				  .builder()
				    .baseUrl("https://api.pushshift.io/reddit/submission/search")
				    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).exchangeStrategies(ExchangeStrategies.builder()
				            .codecs(configurer -> configurer
				                      .defaultCodecs()
				                      .maxInMemorySize(16 * 1024 * 1024))
				                    .build())
				    .defaultUriVariables(Collections.singletonMap("url", "https://api.pushshift.io/reddit/submission/search"))
				  .build();
		ApiResponse response;
		int remaining=0;
		if(size<MAX_API) {
			response=client.get().uri("/?subreddit="+subreddit+"&q="+keywords+"&size="+size+"&sort_type=created_utc").retrieve().bodyToMono(ApiResponse.class).block();
		}else {
			remaining= size-MAX_API;
			response=client.get().uri("/?subreddit="+subreddit+"&q="+keywords+"&size="+MAX_API+"&sort_type=created_utc").retrieve().bodyToMono(ApiResponse.class).block();
		}
		
		List<Post> posts = response.getData();
		if(posts.size()==0) {
			throw new EmptyCollectionException();
		}
		
		while(remaining>0) {
			if(remaining<MAX_API) {
				response=client.get().uri("/?subreddit="+subreddit+"&q="+keywords+"&size="+remaining+"&sort_type=created_utc&before="+posts.get(posts.size()-1).getDateInSeconds()).retrieve().bodyToMono(ApiResponse.class).block();
			}else {
				response=client.get().uri("/?subreddit="+subreddit+"&q="+keywords+"&size="+MAX_API+"&sort_type=created_utc&before="+posts.get(posts.size()-1).getDateInSeconds()).retrieve().bodyToMono(ApiResponse.class).block();
			}
			remaining= remaining-MAX_API;
			posts.addAll(response.getData());
		}
		
		
		return savePosts(posts,collectionName);
	}
	
	private Collection savePosts(List<Post> posts, String collectionName) {
		Collection collection = createCollection(collectionName);
		List<Writing> writings = new ArrayList<>();
		for(Post post:posts) {
			writings.add(new Writing(collection, post.getTitle(), Instant.ofEpochSecond(post.getDateInSeconds()).atZone(ZoneId.systemDefault()).toLocalDateTime(), " reddit post ", post.getText(), post.getAuthor()));
		}
		writingDao.saveAll(writings);
		collection.setWritings(writings);
		collection.setPosts((long)writings.size());
		collectionDao.save(collection);
		return collection;
	}

	@Override
	public Writing findPostById(Long postId, Long userId) throws PermissionException, InstanceNotFoundException {
		Optional<Writing> writingOp =writingDao.findById(postId);
		if(!writingOp.isPresent()) {
			throw new InstanceNotFoundException("project.entities.post", postId);
		}
		Writing writing = writingOp.get();
		if(!permissionDao.findByCollectionIdAndUserId(writing.getCollection().getId(), userId).isPresent() && !internalService.isAdmin(userId))
			throw new PermissionException();
		return writing;
	}

	@Override
	public Block<Writing> findPostsByAuthor(Long userId, String author, int page, int size) throws PermissionException, InstanceNotFoundException {
		Slice<Writing> writings;
		if(internalService.isAdmin(userId)) {
			writings= writingDao.findByAuthor(author,PageRequest.of(page, size));
		}else {
			List<Collection> collections = findPermittedCollections(userId);
			List<Long> collectionsId = new ArrayList<>();
			collections.forEach(collection -> collectionsId.add(collection.getId()));
			writings= writingDao.findByCollectionIdInAndAuthor(collectionsId,author,PageRequest.of(page, size));		
		}
		return new Block<>(prepareStatus(writings.getContent(), userId), writings.hasNext());
	}
	
	private List<Writing> prepareStatus(List<Writing> writings, Long userId) throws InstanceNotFoundException{
		for(Writing writing: writings) {
			if(!internalService.isAdmin(userId)) {
				if(internalService.isPsychologist(userId)) {
					Optional<Psychological_Analysis> analysisOp=psychologicalAnalysisDao.findByUserIdAndWritingId(userId, writing.getId());
					if(!analysisOp.isPresent()) {
						writing.setStatus(0);
					}else {
						Psychological_Analysis analysis = analysisOp.get();
						if(analysis.isFinished()) {
							writing.setStatus(2);
						}else {
							writing.setStatus(1);
						}
					}
				}else {
					Optional<Speech_Analysis> analysisOp=speechAnalysisDao.findByUserIdAndWritingId(userId, writing.getId());
					if(!analysisOp.isPresent()) {
						writing.setStatus(0);
					}else {
						Speech_Analysis analysis = analysisOp.get();
						if(analysis.isFinished()) {
							writing.setStatus(2);
						}else {
							writing.setStatus(1);
						}
					}
				}
				
			}else {
				writing.setStatus(4);
			}
		
		}
		return writings;
	}
	
	
}
