package es.udc.project.backend.model.services;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;

public interface InternalService {
	
	public User checkUser(Long userId) throws InstanceNotFoundException;
	public User checkUser(String username) throws InstanceNotFoundException;
	
	public boolean checkRole(String role)throws InstanceNotFoundException;
	
	public boolean isAdmin(Long userId) throws InstanceNotFoundException;
	
	public boolean isLinguist(Long userId) throws InstanceNotFoundException;
	
	public boolean isPsychologist(Long userId) throws InstanceNotFoundException;
	
	public Collection checkCollection(Long collectionId) throws InstanceNotFoundException;
	
}
