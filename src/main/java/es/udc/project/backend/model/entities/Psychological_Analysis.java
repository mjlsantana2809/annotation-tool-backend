package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Psychological_Analysis extends Analysis {
	public enum Pathology {
		NONE, DETECTED, MORE_INFO_NEEDED
	};
	private String diagnosis;
	private List<Comment> comments;
	private Pathology pathology;
	
	
	public Psychological_Analysis() {}
	
	public Psychological_Analysis(LocalDateTime startDate, LocalDateTime lastModification, String diagnosis,
			boolean finished, User user, Writing writing, Pathology pathology) {
		super( startDate,  lastModification,  finished,  user, writing);
		this.diagnosis = diagnosis;
		this.pathology = pathology;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	@OneToMany(mappedBy = "analysis")
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Pathology getPathology() {
		return pathology;
	}

	public void setPathology(Pathology pathology) {
		this.pathology = pathology;
	}
	
	
	
	
	
	
	
}
