package es.udc.project.backend.model.services;

import org.springframework.web.multipart.MultipartFile;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.exceptions.FileStorageException;

public interface FileStorageService {
	
	public String storeFile(MultipartFile file, String collectionName)throws FileStorageException;
	
	public String createCollectionFolder(String collectionName);
}
