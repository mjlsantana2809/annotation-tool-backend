package es.udc.project.backend.model.exceptions;

@SuppressWarnings("serial")
public class DuplicatedInstanceException extends InstanceException {

    public DuplicatedInstanceException(String name, Object key) {
    	super(name, key); 	
    }
    
}
