package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Collection {
	private Long id;
	private LocalDateTime date;
	private String path;
	private List<Writing> writings;
	private boolean enabled;
	private Long posts;
	
	public Collection() {
	}

	public Collection(LocalDateTime date, String path, boolean enabled, Long posts) {
		this.date = date;
		this.path = path;
		this.enabled = enabled;
		this.posts= posts;
	}
	
	public Collection(LocalDateTime date, String path, List<Writing> writings) {
		this.date = date;
		this.path = path;
		this.writings = writings;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@OneToMany(mappedBy = "collection")
	public List<Writing> getWritings() {
		return writings;
	}

	public void setWritings(List<Writing> writings) {
		this.writings = writings;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Long getPosts() {
		return posts;
	}

	public void setPosts(Long posts) {
		this.posts = posts;
	}

	
	
	
	
}
