package es.udc.project.backend.model.services;

import java.util.List;

import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PasswordNotStrongException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.exceptions.IncorrectPasswordException;
import es.udc.project.backend.model.exceptions.IncorrectLoginException;



public interface UserService {

	void signUp(User user, boolean sendEmail) throws DuplicatedInstanceException;
	
	User login(String userName, String password) throws IncorrectLoginException;

	User loginFromId(Long id) throws InstanceNotFoundException;
	
	void changePassword(Long id, String oldPassword, String newPassword)
			throws InstanceNotFoundException, IncorrectPasswordException, PasswordNotStrongException;
	
	Block<User> findUsers(Long userId, int page, int size) throws InstanceNotFoundException, PermissionException;
	
	void updateProfile(Long id, String firstName, String lastName, String email) throws InstanceNotFoundException;
}
