package es.udc.project.backend.model.services;

import java.util.List;

import es.udc.project.backend.model.entities.Fragment;
import es.udc.project.backend.model.entities.Fragment_Fragment;
import es.udc.project.backend.model.entities.Fragment_Group;
import es.udc.project.backend.model.entities.Group;
import es.udc.project.backend.model.entities.Group_Group;
import es.udc.project.backend.model.entities.Speech_Analysis;

public class SpeechAnalysisResult {
	private Speech_Analysis analysis;
	private List<Fragment> fragments;
	private List<Group> groups;
	private List<Fragment_Fragment> fxf;
	private List <Fragment_Group> fxg;
	private List<Group_Group> gxg;
	
	public SpeechAnalysisResult() {

	}

	
	public SpeechAnalysisResult(Speech_Analysis analysis, List<Fragment> fragments, List<Group> groups,
			List<Fragment_Fragment> fxf, List<Fragment_Group> fxg, List<Group_Group> gxg) {
		this.analysis = analysis;
		this.fragments = fragments;
		this.groups = groups;
		this.fxf = fxf;
		this.fxg = fxg;
		this.gxg = gxg;
	}

	public Speech_Analysis getAnalysis() {
		return analysis;
	}

	public void setAnalysis(Speech_Analysis analysis) {
		this.analysis = analysis;
	}

	public List<Fragment> getFragments() {
		return fragments;
	}

	public void setFragments(List<Fragment> fragments) {
		this.fragments = fragments;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Fragment_Fragment> getFxf() {
		return fxf;
	}

	public void setFxf(List<Fragment_Fragment> fxf) {
		this.fxf = fxf;
	}

	public List<Fragment_Group> getFxg() {
		return fxg;
	}

	public void setFxg(List<Fragment_Group> fxg) {
		this.fxg = fxg;
	}

	public List<Group_Group> getGxg() {
		return gxg;
	}

	public void setGxg(List<Group_Group> gxg) {
		this.gxg = gxg;
	}
	
	
}
