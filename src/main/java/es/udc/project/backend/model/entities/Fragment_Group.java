package es.udc.project.backend.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Fragment_Group {
	private Long id;
	private Fragment fragment;
	@Column(name = "FGroup", nullable = false)
	private Group group;
	private boolean fragmentSource;
	private Relation_Type relation;
	
	public Fragment_Group() {
	}
	
	public Fragment_Group(Fragment fragment, Group group, boolean fragmentSource, Relation_Type relation) {
	
		this.fragment = fragment;
		this.group = group;
		this.fragmentSource = fragmentSource;
		this.relation = relation;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fragment")
	public Fragment getFragment() {
		return fragment;
	}

	public void setFragment(Fragment fragment) {
		this.fragment = fragment;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fGroup")
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public boolean isFragmentSource() {
		return fragmentSource;
	}

	public void setFragmentSource(boolean fragmentSource) {
		this.fragmentSource = fragmentSource;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "relation")
	public Relation_Type getRelation() {
		return relation;
	}

	public void setRelation(Relation_Type relation) {
		this.relation = relation;
	}
	
	
	
}
