package es.udc.project.backend.model.entities;

import java.util.List;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface GroupDao extends PagingAndSortingRepository<Group, String> {
	List<Group> findByAnalysisId(Long analysisId); 
}