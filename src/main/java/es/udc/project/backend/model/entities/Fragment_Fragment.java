package es.udc.project.backend.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Fragment_Fragment {
	private Long id;
	private Fragment fragmentFrom;
	private Fragment fragmentTo;
	private Relation_Type relation;
	
	
	public Fragment_Fragment() {
	}
	
	public Fragment_Fragment(Fragment fragmentFrom, Fragment fragmentTo, Relation_Type relation) {
		this.fragmentFrom = fragmentFrom;
		this.fragmentTo = fragmentTo;
		this.relation = relation;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fragmentFrom")
	public Fragment getFragmentFrom() {
		return fragmentFrom;
	}

	public void setFragmentFrom(Fragment fragmentFrom) {
		this.fragmentFrom = fragmentFrom;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fragmentTo")
	public Fragment getFragmentTo() {
		return fragmentTo;
	}

	public void setFragmentTo(Fragment fragmentTo) {
		this.fragmentTo = fragmentTo;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "relation")
	public Relation_Type getRelation() {
		return relation;
	}

	public void setRelation(Relation_Type relation) {
		this.relation = relation;
	}
	
	
	
}
