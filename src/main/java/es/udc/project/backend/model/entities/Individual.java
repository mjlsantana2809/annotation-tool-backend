package es.udc.project.backend.model.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "INDIVIDUAL")
public class Individual {

	private String author;
	private List<Writing> writings;
	
	public Individual() {
	}
	
	public Individual(String author, List<Writing> writings) {
		super();
		this.author = author;
		this.writings = writings;
	}
	@XmlElement(name = "ID")
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@XmlElement(name = "WRITING")
	public List<Writing> getWritings() {
		return writings;
	}
	public void setWritings(List<Writing> writings) {
		this.writings = writings;
	}
	
	
}