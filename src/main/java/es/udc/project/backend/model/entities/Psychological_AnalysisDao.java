package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface Psychological_AnalysisDao extends PagingAndSortingRepository<Psychological_Analysis, Long> {
	Optional<Psychological_Analysis> findByUserIdAndWritingId(Long userId, Long writingId);
	List<Psychological_Analysis> findByUserIdAndFinishedOrderByLastModificationDesc(Long userId, boolean finished);
	Slice<Psychological_Analysis> findByUserIdOrderByLastModificationDesc(Long userId, Pageable pageable);
	@Query("select a.pathology, count(*) from Psychological_Analysis a group by a.pathology")
	List<Object[]>countByPathology();
	
	Long countByWritingCollectionId(Long collectionId);
	@Query("select a.pathology, count(*) from Psychological_Analysis a where a.writing.collection.id= ?1  group by a.pathology")
	List<Object[]>countByPathology(Long collectionId);
}