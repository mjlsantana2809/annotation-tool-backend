package es.udc.project.backend.model.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Group_Group {
	private Long id;
	private Group groupFrom;
	private Group groupTo;
	private Relation_Type relation;
	
	public Group_Group() {
	}
	
	public Group_Group( Group groupFrom, Group groupTo, Relation_Type relation) {
		this.groupFrom = groupFrom;
		this.groupTo = groupTo;
		this.relation = relation;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "groupFrom")
	public Group getGroupFrom() {
		return groupFrom;
	}

	public void setGroupFrom(Group groupFrom) {
		this.groupFrom = groupFrom;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "groupTo")
	public Group getGroupTo() {
		return groupTo;
	}

	public void setGroupTo(Group groupTo) {
		this.groupTo = groupTo;
	}
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "relation")
	public Relation_Type getRelation() {
		return relation;
	}

	public void setRelation(Relation_Type relation) {
		this.relation = relation;
	}
	
	
}
