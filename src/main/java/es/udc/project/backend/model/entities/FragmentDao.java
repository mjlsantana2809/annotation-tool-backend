package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface FragmentDao extends PagingAndSortingRepository<Fragment, String> {
	Optional<Fragment> findByAnalysisIdAndStartAndEndAndTitle(Long analysisId, Long start, Long end, boolean title);
	List<Fragment> findByAnalysisId(Long analysisId); 
	List<Fragment> findByGroupId(Long groupId);
}