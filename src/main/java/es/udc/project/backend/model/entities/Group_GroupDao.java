package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface Group_GroupDao extends PagingAndSortingRepository<Group_Group, Long> {
	void deleteByGroupFromIdAndGroupToId(String groupFromId, String groupToId);
	List<Group_Group> findByGroupFromAnalysisId(Long groupFromAnalysisId);
	
	@Query("select a.relation.name ,count(*) from Group_Group a  group by a.relation.name")
	 List<Object[]>countRelations();
	 
	 
	 @Query("select a.relation.name ,count(*) from Group_Group a where a.groupFrom.analysis.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByAnalysis(Long analysisId);
	 Long countByGroupFromAnalysisId(Long analysisId);
	 
	 @Query("select a.relation.name ,count(*) from Group_Group a where a.groupFrom.analysis.writing.collection.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByCollection(Long collectionId);
	 Long countByGroupFromAnalysisWritingCollectionId(Long collectionId);
	
}