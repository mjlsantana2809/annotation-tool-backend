package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PermissionDao extends PagingAndSortingRepository<Permission, Long> {
	Optional<Permission> findByCollectionIdAndUserId(Long collectionId, Long userId);
	Optional<Permission> findBySpeechAnalysisIdAndUserId(Long speechAnalysisId, Long userId);
	Optional<Permission> findByPsychologicalAnalysisIdAndUserId(Long psychologicalAnalysisId, Long userId);
	
	List<Permission> findByUserIdAndCollectionNotNull(Long userId);
	//Los que un usuario ha recibido
	Slice<Permission> findByUserIdAndType(Long userId, String type, Pageable pageable);
	
	Slice<Permission> findBySpeechAnalysisUserIdAndType(Long speechAnalysisUserId, String type, Pageable pageable);
	Slice<Permission> findByPsychologicalAnalysisUserIdAndType(Long psychologicalAnalysisUserId, String type, Pageable pageable);
}