package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Analysis {
	
	private Long id;
	private LocalDateTime startDate;
	private LocalDateTime lastModification;
	private boolean finished;
	private User user;
	private Writing writing;
	
	public Analysis() {
	}
	
	
	public Analysis(LocalDateTime startDate, LocalDateTime lastModification, boolean finished, User user,
			Writing writing) {
		this.startDate = startDate;
		this.lastModification = lastModification;
		this.finished = finished;
		this.user = user;
		this.writing = writing;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public LocalDateTime getStartDate() {
		return startDate;
	}


	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}


	public LocalDateTime getLastModification() {
		return lastModification;
	}


	public void setLastModification(LocalDateTime lastModification) {
		this.lastModification = lastModification;
	}


	public boolean isFinished() {
		return finished;
	}


	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "writing")
	public Writing getWriting() {
		return writing;
	}


	public void setWriting(Writing writing) {
		this.writing = writing;
	}
	
	
	
}
