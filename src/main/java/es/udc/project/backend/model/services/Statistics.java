package es.udc.project.backend.model.services;

import java.util.List;

public class Statistics {
	private List<Object[]> data;
	private long total;
	
	
	
	public Statistics(List<Object[]> data, long total) {
		this.data = data;
		this.total = total;
	}
	public List<Object[]> getData() {
		return data;
	}
	public void setData(List<Object[]> data) {
		this.data = data;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	
	
}
