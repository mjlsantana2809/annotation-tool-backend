package es.udc.project.backend.model.entities;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Speech_Analysis extends Analysis {
	
	
	private Methodology methodology;
	
	public Speech_Analysis() {
	}
	
	public Speech_Analysis( LocalDateTime startDate, LocalDateTime lastModification, boolean finished,
			User user, Writing writing, Methodology methodology) {
		super( startDate,  lastModification,  finished,  user, writing);
		this.methodology= methodology;
	}
	
	
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "methodology") 
	public Methodology getMethodology() {
		return methodology;
	}

	public void setMethodology(Methodology methodology) {
		this.methodology = methodology;
	}
	
	
	
}
