package es.udc.project.backend.model.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.CollectionDao;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.UserDao;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import io.jsonwebtoken.lang.Collections;

@Service
@Transactional(readOnly=true)
public class InternalServiceImpl implements InternalService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private CollectionDao collectionDao;
	



	@Override
	public User checkUser(Long userId) throws InstanceNotFoundException {

		Optional<User> user = userDao.findById(userId);
		
		if (!user.isPresent()) {
			throw new InstanceNotFoundException("project.entities.user", userId);
		}
		
		return user.get();
		
	}


	@Override
	public boolean checkRole(String role) throws InstanceNotFoundException {
		RoleType[] roles = RoleType.values();
		for(RoleType aux:roles) {
			if(aux.name().toLowerCase().equals(role.toLowerCase()))
				return true;
		}
			throw new InstanceNotFoundException("project.entities.Role", role);
	}


	@Override
	public boolean isAdmin(Long userId) throws  InstanceNotFoundException {
		Optional<User> userO = userDao.findById(userId);
		if (!userO.isPresent()) {
			throw new InstanceNotFoundException("project.entities.user", userId);
		}
		User user = userO.get();
		if(user.getRole().name()!="ADMIN") {
			return false;
		}
		return true;

	}
	
	@Override
	public Collection checkCollection(Long collectionId) throws InstanceNotFoundException {

		Optional<Collection> collection = collectionDao.findById(collectionId);
		
		if (!collection.isPresent()) {
			throw new InstanceNotFoundException("project.entities.collection", collectionId);
		}
		
		return collection.get();
		
	}


	@Override
	public boolean isLinguist(Long userId) throws InstanceNotFoundException {
		Optional<User> userO = userDao.findById(userId);
		if (!userO.isPresent()) {
			throw new InstanceNotFoundException("project.entities.user", userId);
		}
		User user = userO.get();
		if(user.getRole().name()!="LINGUIST") {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean isPsychologist(Long userId) throws InstanceNotFoundException {
		Optional<User> userO = userDao.findById(userId);
		if (!userO.isPresent()) {
			throw new InstanceNotFoundException("project.entities.user", userId);
		}
		User user = userO.get();
		if(user.getRole().name()!="PSYCHOLOGIST") {
			return false;
		}
		return true;
	}


	@Override
	public User checkUser(String username) throws InstanceNotFoundException {
		Optional<User> user = userDao.findByUserName(username);
		
		if (!user.isPresent()) {
			throw new InstanceNotFoundException("project.entities.user", username);
		}
		
		return user.get();
	}

}
