package es.udc.project.backend.model.services;

import java.util.List;

import es.udc.project.backend.model.entities.Analysis;
import es.udc.project.backend.model.entities.Comment;
import es.udc.project.backend.model.entities.Methodology;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Relation_Type;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadySharedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.CommentOutOfBoundsException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.InvalidInputException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.rest.dtos.params.CommentParamsDto;
import es.udc.project.backend.rest.dtos.params.PsychologicalAnalysisParamsDto;
import es.udc.project.backend.rest.dtos.params.SpeechAnalysisParamsDto;

public interface StatisticsService {

	public Statistics countByPathology(Long collectionId);
	
	public Statistics countRelations();
	
	public Statistics countRelationsByAnalysis(Long analysisId);
	
	public Statistics countRelationsByCollection(Long collectionId);
	
}
