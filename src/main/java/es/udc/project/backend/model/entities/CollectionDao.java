package es.udc.project.backend.model.entities;

import java.util.List;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface CollectionDao extends PagingAndSortingRepository<Collection, Long> {
	
	
	@Query(value ="SELECT c FROM Collection c WHERE c.id IN :ids")
	Slice<Collection> findPermittedCollections(@Param("ids") List<Long> ids);
	
	Slice<Collection> findByEnabled(boolean enabled);
	
}