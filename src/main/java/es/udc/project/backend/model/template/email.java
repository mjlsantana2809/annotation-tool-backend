package es.udc.project.backend.model.template;

public class email {

	
	public static String fillTemplate(String cleanPassword,String username,String name) {
		String template="<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n" + 
				"<html>\r\n" + 
				"  <head>\r\n" + 
				"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n" + 
				"    <title>Mailto</title>\r\n" + 
				"   <link href=\"https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800\" rel=\"stylesheet\">\r\n" + 
				"    <link href=\"https://use.fontawesome.com/releases/v5.0.6/css/all.css\" rel=\"stylesheet\">" + 
				"    <style type=\"text/css\">\r\n" + 
				"      html {\r\n" + 
				"        -webkit-text-size-adjust: none;\r\n" + 
				"        -ms-text-size-adjust: none;\r\n" + 
				"      }\r\n" + 
				"      @media only screen and (min-device-width: 750px) {\r\n" + 
				"        .table750 {\r\n" + 
				"          width: 750px !important;\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"      @media only screen and (max-device-width: 750px),\r\n" + 
				"        only screen and (max-width: 750px) {\r\n" + 
				"        table[class=\"table750\"] {\r\n" + 
				"          width: 100% !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_b {\r\n" + 
				"          width: 93% !important;\r\n" + 
				"          max-width: 93% !important;\r\n" + 
				"          min-width: 93% !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_b1 {\r\n" + 
				"          width: 100% !important;\r\n" + 
				"          max-width: 100% !important;\r\n" + 
				"          min-width: 100% !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_left {\r\n" + 
				"          text-align: left !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_soc {\r\n" + 
				"          width: 50% !important;\r\n" + 
				"          max-width: 50% !important;\r\n" + 
				"          min-width: 50% !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_menu {\r\n" + 
				"          width: 50% !important;\r\n" + 
				"          max-width: 50% !important;\r\n" + 
				"          min-width: 50% !important;\r\n" + 
				"          box-shadow: inset -1px -1px 0 0 rgba(255, 255, 255, 0.2);\r\n" + 
				"        }\r\n" + 
				"        .mob_center {\r\n" + 
				"          text-align: center !important;\r\n" + 
				"        }\r\n" + 
				"        .top_pad {\r\n" + 
				"          height: 15px !important;\r\n" + 
				"          max-height: 15px !important;\r\n" + 
				"          min-height: 15px !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_pad {\r\n" + 
				"          width: 15px !important;\r\n" + 
				"          max-width: 15px !important;\r\n" + 
				"          min-width: 15px !important;\r\n" + 
				"        }\r\n" + 
				"        .mob_div {\r\n" + 
				"          display: block !important;\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"      @media only screen and (max-device-width: 550px),\r\n" + 
				"        only screen and (max-width: 550px) {\r\n" + 
				"        .mod_div {\r\n" + 
				"          display: block !important;\r\n" + 
				"        }\r\n" + 
				"      }\r\n" + 
				"      .table750 {\r\n" + 
				"        width: 750px;\r\n" + 
				"      }\r\n" + 
				"    </style>\r\n" + 
				"  </head>\r\n" + 
				"  <body style=\"margin: 0; padding: 0;\">\r\n" + 
				"    <table\r\n" + 
				"      cellpadding=\"0\"\r\n" + 
				"      cellspacing=\"0\"\r\n" + 
				"      border=\"0\"\r\n" + 
				"      width=\"100%\"\r\n" + 
				"      style=\"\r\n" + 
				"        background: #f3f3f3;\r\n" + 
				"        min-width: 350px;\r\n" + 
				"        font-size: 1px;\r\n" + 
				"        line-height: normal;\r\n" + 
				"      \"\r\n" + 
				"    >\r\n" + 
				"      <tr>\r\n" + 
				"        <td align=\"center\" valign=\"top\">\r\n" + 
				"          <!--[if (gte mso 9)|(IE)]>         <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">         <tr><td align=\"center\" valign=\"top\" width=\"750\"><![endif]-->\r\n" + 
				"          <table\r\n" + 
				"            cellpadding=\"0\"\r\n" + 
				"            cellspacing=\"0\"\r\n" + 
				"            border=\"0\"\r\n" + 
				"            width=\"750\"\r\n" + 
				"            class=\"table750\"\r\n" + 
				"            style=\"\r\n" + 
				"              width: 100%;\r\n" + 
				"              max-width: 750px;\r\n" + 
				"              min-width: 350px;\r\n" + 
				"              background: #f3f3f3;\r\n" + 
				"            \"\r\n" + 
				"          >\r\n" + 
				"            <tr>\r\n" + 
				"              <td\r\n" + 
				"                class=\"mob_pad\"\r\n" + 
				"                width=\"25\"\r\n" + 
				"                style=\"width: 25px; max-width: 25px; min-width: 25px;\"\r\n" + 
				"              ></td>\r\n" + 
				"              <td align=\"center\" valign=\"top\" style=\"background: #ffffff;\">\r\n" + 
				"                <!-- ESPACIADO SUPERIOR -->\r\n" + 
				"                <table\r\n" + 
				"                  cellpadding=\"0\"\r\n" + 
				"                  cellspacing=\"0\"\r\n" + 
				"                  border=\"0\"\r\n" + 
				"                  width=\"100%\"\r\n" + 
				"                  style=\"\r\n" + 
				"                    width: 100% !important;\r\n" + 
				"                    min-width: 100%;\r\n" + 
				"                    max-width: 100%;\r\n" + 
				"                    background: #f3f3f3;\r\n" + 
				"                  \"\r\n" + 
				"                >\r\n" + 
				"                  <tr>\r\n" + 
				"                    <td align=\"right\" valign=\"top\">\r\n" + 
				"                      <div\r\n" + 
				"                        class=\"top_pad\"\r\n" + 
				"                        style=\"\r\n" + 
				"                          height: 25px;\r\n" + 
				"                          line-height: 25px;\r\n" + 
				"                          font-size: 23px;\r\n" + 
				"                        \"\r\n" + 
				"                      ></div>\r\n" + 
				"                    </td>\r\n" + 
				"                  </tr>\r\n" + 
				"                </table>\r\n" + 
				"                ;\r\n" + 
				"                <!-- TEXTO -->\r\n" + 
				"                <table\r\n" + 
				"                  cellpadding=\"0\"\r\n" + 
				"                  cellspacing=\"0\"\r\n" + 
				"                  border=\"0\"\r\n" + 
				"                  width=\"88%\"\r\n" + 
				"                  style=\"width: 88% !important; min-width: 88%; max-width: 88%;\"\r\n" + 
				"                >\r\n" + 
				"                  <tr>\r\n" + 
				"                    <td align=\"left\" valign=\"top\">\r\n" + 
				"                      <font\r\n" + 
				"                        face=\"\\Source Sans Pro\\, sans-serif\"\r\n" + 
				"                        color=\"#1a1a1a\"\r\n" + 
				"                        style=\"\r\n" + 
				"                          font-size: 52px;\r\n" + 
				"                          line-height: 60px;\r\n" + 
				"                          font-weight: 300;\r\n" + 
				"                          letter-spacing: -1.5px;\r\n" + 
				"                        \"\r\n" + 
				"                      >\r\n" + 
				"                        <span\r\n" + 
				"                          style=\"\r\n" + 
				"                            font-family: \\SourceSansPro\\, Arial, Tahoma, Geneva,\r\n" + 
				"                              sans-serif;\r\n" + 
				"                            color: #1a1a1a;\r\n" + 
				"                            font-size: 52px;\r\n" + 
				"                            line-height: 60px;\r\n" + 
				"                            font-weight: 300;\r\n" + 
				"                            letter-spacing: -1.5px;\r\n" + 
				"                          \"\r\n" + 
				"                          >Hola <span style=\"color: indigo;\"\r\n" + 
				"                          ><strong>\r\n" + 
				"                           "+name+"\r\n" + 
				"                          </strong></span\r\n" + 
				"                        ></span\r\n" + 
				"                        >\r\n" + 
				"                      </font>\r\n" + 
				"                      <div\r\n" + 
				"                        style=\"\r\n" + 
				"                          height: 33px;\r\n" + 
				"                          line-height: 33px;\r\n" + 
				"                          font-size: 31px;\r\n" + 
				"                        \"\r\n" + 
				"                      ></div>\r\n" + 
				"                      <font\r\n" + 
				"                        face=\"\\Source Sans Pro\\, sans-serif\"\r\n" + 
				"                        color=\"#585858\"\r\n" + 
				"                        style=\"font-size: 24px; line-height: 32px;\"\r\n" + 
				"                      >\r\n" + 
				"                        <span\r\n" + 
				"                          style=\"\r\n" + 
				"                            font-family: \\SourceSansPro\\, Arial, Tahoma, Geneva,\r\n" + 
				"                              sans-serif;\r\n" + 
				"                            color: #585858;\r\n" + 
				"                            font-size: 24px;\r\n" + 
				"                            line-height: 32px;\r\n" + 
				"                          \"\r\n" + 
				"                          >Nos complace informarte que has sido invitado\r\n" + 
				"                          a usar la aplicación \"AnnotationTool\" a traves de las credenciales:\r\n" + 
				"                         \r\n" + 
				"                          <br> <br> <ul> <li>userName: "+username + "\r\n </li>" + 
				"                          <li>password: "+cleanPassword+ "</span\r\n" + 
				"                        >\r\n</li></ul>" + 
				"                      </font>\r\n" + 
				"                      <div\r\n" + 
				"                        style=\"\r\n" + 
				"                          height: 20px;\r\n" + 
				"                          line-height: 20px;\r\n" + 
				"                          font-size: 18px;\r\n" + 
				"                        \"\r\n" + 
				"                      ></div>\r\n" + 
				"                      <div\r\n" + 
				"                        style=\"\r\n" + 
				"                          height: 33px;\r\n" + 
				"                          line-height: 33px;\r\n" + 
				"                          font-size: 31px;\r\n" + 
				"                        \"\r\n" + 
				"                      ></div>\r\n" + 
				"                     \r\n" + 
				"                      <div\r\n" + 
				"                        style=\"\r\n" + 
				"                          height: 55px;\r\n" + 
				"                          line-height: 75px;\r\n" + 
				"                          font-size: 73px;\r\n" + 
				"                        \"\r\n" + 
				"                      ></div>\r\n" + 
				"                    </td>\r\n" + 
				"                  </tr>\r\n" + 
				"                </table>\r\n" + 
				"                <!-- FOOTER -->              \r\n" + 
				"              </td>\r\n" + 
				"              <td\r\n" + 
				"                class=\"mob_pad\"\r\n" + 
				"                width=\"25\"\r\n" + 
				"                style=\"width: 25px; max-width: 25px; min-width: 25px;\"\r\n" + 
				"              ></td>\r\n" + 
				"            </tr>\r\n" + 
				"          </table>\r\n" + 
				"          <!--[if (gte mso 9)|(IE)]>         </td></tr>         </table><![endif]-->\r\n" + 
				"        </td>\r\n" + 
				"      </tr>\r\n" + 
				"    </table>\r\n" + 
				"  </body>\r\n" + 
				"</html>\r\n" + 
				"";
		return template;
	}
	
}
