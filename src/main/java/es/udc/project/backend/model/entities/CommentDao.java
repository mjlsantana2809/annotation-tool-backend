package es.udc.project.backend.model.entities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface CommentDao extends PagingAndSortingRepository<Comment, Long> {
	
	Optional<Comment> findByAnalysisIdAndStartAndEnd(Long analysisId, Long start, Long end);
	long deleteByAnalysisIdAndStartAndEnd(Long analysisId, Long start, Long end);
	List<Comment> findByAnalysisId(Long analysisId);
}