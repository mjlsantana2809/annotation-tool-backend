package es.udc.project.backend.model.entities;

import java.util.List;

import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface Fragment_FragmentDao extends PagingAndSortingRepository<Fragment_Fragment, Long> {
	
	 void deleteByFragmentFromIdAndFragmentToId(String fragmentFromId, String fragmentToId);
	 List<Fragment_Fragment> findByFragmentFromAnalysisId(Long fragmentFromAnalysisId);
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Fragment a  group by a.relation.name")
	 List<Object[]>countRelations();
	 
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Fragment a where a.fragmentFrom.analysis.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByAnalysis(Long analysisId);
	 Long countByFragmentFromAnalysisId(Long analysisId);
	 
	 @Query("select a.relation.name ,count(*) from Fragment_Fragment a where a.fragmentFrom.analysis.writing.collection.id=?1 group by a.relation.name")
	 List<Object[]>countRelationsByCollection(Long collectionId);
	 Long countByFragmentFromAnalysisWritingCollectionId(Long collectionId);
	 
}