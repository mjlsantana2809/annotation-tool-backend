package es.udc.project.backend.model.services;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Post {

	private String title;
	private String author;
	private String text;
	private Long dateInSeconds; 
	
	public Post() {
	}
	
	
	public Post(String title, String author, String text, Long dateInSeconds) {
		super();
		this.title = title;
		this.author = author;
		this.text = text;
		this.dateInSeconds = dateInSeconds;
	}


	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}
	
	@JsonProperty("author")
	public String getAuthor() {
		return author;
	}
	@JsonProperty("author")
	public void setAuthor(String author) {
		this.author = author;
	}
	
	@JsonProperty("selftext")
	public String getText() {
		return text;
	}
	@JsonProperty("selftext")
	public void setText(String text) {
		this.text = text;
	}

	@JsonProperty("created_utc")
	public Long getDateInSeconds() {
		return dateInSeconds;
	}


	public void setDateInSeconds(Long dateInSeconds) {
		this.dateInSeconds = dateInSeconds;
	}
	
	
	
	
}
