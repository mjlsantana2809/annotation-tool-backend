package es.udc.project.backend.model.entities;

import java.util.List;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "FGroup")
public class Group {
	private String id;
	private Speech_Analysis analysis;
	private String groupLabel;
	private Long posX;
	private Long posY;
	private Group group;

	
	public Group() {
	}
	
	
	
	public Group(String id, Speech_Analysis analysis, String groupLabel, Long posX, Long posY) {
		this.id= id;
		this.analysis = analysis;
		this.groupLabel = groupLabel;
		this.posX = posX;
		this.posY = posY;
	}

	public Group(String id, Speech_Analysis analysis, String groupLabel, Long posX, Long posY, Group group) {
		super();
		this.id=id;
		this.analysis = analysis;
		this.groupLabel = groupLabel;
		this.posX = posX;
		this.posY = posY;
		this.group = group;
	}



	@Id
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "analysis")
	public Speech_Analysis getAnalysis() {
		return analysis;
	}
	public void setAnalysis(Speech_Analysis analysis) {
		this.analysis = analysis;
	}
	public String getGroupLabel() {
		return groupLabel;
	}
	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}

	public Long getPosX() {
		return posX;
	}



	public void setPosX(Long posX) {
		this.posX = posX;
	}



	public Long getPosY() {
		return posY;
	}



	public void setPosY(Long posY) {
		this.posY = posY;
	}


	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "fGroup")
	public Group getGroup() {
		return group;
	}



	public void setGroup(Group group) {
		this.group = group;
	}

	
	
}
