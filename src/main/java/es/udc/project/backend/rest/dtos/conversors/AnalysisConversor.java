package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Psychological_Analysis;
import es.udc.project.backend.model.entities.Speech_Analysis;
import es.udc.project.backend.rest.dtos.AnalysisDto;
import static es.udc.project.backend.rest.dtos.conversors.PostConversor.toPostDto;

public class AnalysisConversor {
	public static final AnalysisDto toAnalysisDto(Speech_Analysis analysis) {
		return new AnalysisDto(analysis.getId(), toPostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished(), analysis.getUser().getId());
	}

	public static final List<AnalysisDto> speechAnalysisToAnalysisDtos(List<Speech_Analysis> analysis) {
		return analysis.stream().map(a -> toAnalysisDto(a)).collect(Collectors.toList());
	}
	
	public static final AnalysisDto toAnalysisDto(Psychological_Analysis analysis) {
		return new AnalysisDto(analysis.getId(), toPostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished(), analysis.getUser().getId());
	}

	public static final List<AnalysisDto> psychologicalAnalysisToAnalysisDtos(List<Psychological_Analysis> analysis) {
		return analysis.stream().map(a -> toAnalysisDto(a)).collect(Collectors.toList());
	}
}
