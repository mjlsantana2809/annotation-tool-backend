package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="Analysis")
public class PsychologicalAnalysisDto extends AnalysisDto {
	
	private String diagnosis;
	private List<CommentDto> comments;
	private int pathology;
	
	
	public PsychologicalAnalysisDto() {
		
	}
	
	public PsychologicalAnalysisDto(Long id, PostDto post, LocalDateTime lastModification, boolean finished,String diagnosis, List<CommentDto> comments, Long userId, int pathology) {
		super(id, post, lastModification,finished,userId);
		this.diagnosis=diagnosis;
		this.comments=comments;
		this.pathology=pathology;
	}
	
	
	@XmlElement(name="Diagnosis")
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	@XmlElement(name="Comment")
	public List<CommentDto> getComments() {
		return comments;
	}
	public void setComments(List<CommentDto> comments) {
		this.comments = comments;
	}
	@XmlElement(name="Pathology")
	public int getPathology() {
		return pathology;
	}

	public void setPathology(int pathology) {
		this.pathology = pathology;
	}
	
	
	
	
	
	

	
}
