package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.services.AnalysisService;
import es.udc.project.backend.rest.dtos.PostListDto;

public class PostListConversor {
	
public static final PostListDto toPostListDto(Writing writing) {
	Collection collection = writing.getCollection();
	String[] collectionPath = collection.getPath().split("/");
	
	if(writing.getTitle().trim().equals("")) {
		if(writing.getText().length()<100) {
		
			return new PostListDto(writing.getId(),"<NO TITLE> "+writing.getText().substring(0,writing.getText().length()-1),writing.getAuthor(),writing.getStatus(),collectionPath[collectionPath.length-1],collection.getId());
		}else {
			return new PostListDto(writing.getId(),"<NO TITLE> "+ writing.getText().substring(0,99),writing.getAuthor(),writing.getStatus(),collectionPath[collectionPath.length-1],collection.getId());
		}
		
	}else {
		return new PostListDto(writing.getId(),writing.getTitle(),writing.getAuthor(),writing.getStatus(),collectionPath[collectionPath.length-1],collection.getId());
	}
	
}

public static final List<PostListDto> toPostListDtos(List<Writing> writings) {
	return writings.stream().map(w -> toPostListDto(w)).collect(Collectors.toList());
}

	

}
