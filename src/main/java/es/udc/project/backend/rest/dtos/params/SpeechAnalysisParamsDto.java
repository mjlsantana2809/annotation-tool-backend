package es.udc.project.backend.rest.dtos.params;

import java.util.List;

public class SpeechAnalysisParamsDto {
	private long id;
	private List<SegmentParamsDto> segments;
	private List<GroupParamsDto> groups;
	private List<RelationParamsDto> relations;

	public SpeechAnalysisParamsDto(long id, List<SegmentParamsDto> segments, List<GroupParamsDto> groups,
			List<RelationParamsDto> relations) {
		this.id = id;
		this.segments = segments;
		this.groups = groups;
		this.relations = relations;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public List<SegmentParamsDto> getSegments() {
		return segments;
	}


	public void setSegments(List<SegmentParamsDto> segments) {
		this.segments = segments;
	}


	public List<GroupParamsDto> getGroups() {
		return groups;
	}


	public void setGroups(List<GroupParamsDto> groups) {
		this.groups = groups;
	}


	public List<RelationParamsDto> getRelations() {
		return relations;
	}


	public void setRelations(List<RelationParamsDto> relaciones) {
		this.relations = relaciones;
	}
	
	
}
