package es.udc.project.backend.rest.dtos;

import es.udc.project.backend.model.entities.Relation_Type;

public class RelationCountDto {
	private String relation;
	private long count;
	
	public RelationCountDto() {

	}
	
	public RelationCountDto(String relation, long count) {
		this.relation = relation;
		this.count = count;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	
	
	
}
