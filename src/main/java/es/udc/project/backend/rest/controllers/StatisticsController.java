package es.udc.project.backend.rest.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static es.udc.project.backend.rest.dtos.conversors.RelationCountConversor.toRelationCountDtos;

import es.udc.project.backend.model.entities.Psychological_Analysis.Pathology;
import es.udc.project.backend.model.services.Statistics;
import es.udc.project.backend.model.services.StatisticsService;
import es.udc.project.backend.rest.dtos.PathologyCountDto;
import es.udc.project.backend.rest.dtos.StatisticsDto;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

	@Autowired
	private StatisticsService service;
	
	@GetMapping("/countByPathology")
	public StatisticsDto countByPathology(@RequestParam(required = false) Long collectionId) {
		Statistics content= service.countByPathology(collectionId);
		List<PathologyCountDto> pathologyCount= new ArrayList<>();
		for(Object[] result:content.getData()) {
			Pathology pathology= (Pathology) result[0];
			long count= ((Number) result[1]).longValue();
			pathologyCount.add( new PathologyCountDto(pathology.name(), pathology.ordinal(), count));
			
		}
		
		return new StatisticsDto(pathologyCount, content.getTotal());
		
	}
	
	
	@GetMapping("/relations")
	public StatisticsDto countRelations() {
		Statistics content= service.countRelations();
		return new StatisticsDto( content.getTotal(),toRelationCountDtos(content.getData()));
		
	}
	@GetMapping("/relations/analysis/{analysisId}")
	public StatisticsDto countRelationsByAnalysis(@PathVariable Long analysisId) {
		Statistics content= service.countRelationsByAnalysis(analysisId);
		return new StatisticsDto( content.getTotal(),toRelationCountDtos(content.getData()));
		
	}
	@GetMapping("/relations/collection/{collectionId}")
	public StatisticsDto countRelationsByCollection(@PathVariable Long collectionId) {
		Statistics content= service.countRelationsByCollection(collectionId);
		return new StatisticsDto( content.getTotal(),toRelationCountDtos(content.getData()));
		
	}
}
