package es.udc.project.backend.rest.dtos.params;

public class CommentParamsDto {
	private Long start;
	private Long end;
	private String text;
	//Status:
	// 0 -> persistent; 1 -> new; 2 -> deleted
	private int status;
	private String fragment;
	private boolean title;
	
	public CommentParamsDto(Long start, Long end, String text, int status, String fragment, boolean title) {
		this.start = start;
		this.end = end;
		this.text = text;
		this.status = status;
		this.fragment = fragment;
		this.title= title;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFragment() {
		return fragment;
	}

	public void setFragment(String fragment) {
		this.fragment = fragment;
	}

	public boolean isTitle() {
		return title;
	}

	public void setTitle(boolean title) {
		this.title = title;
	}
	
	
	
	
	
}
