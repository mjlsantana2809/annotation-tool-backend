package es.udc.project.backend.rest.controllers;


import java.net.URI;
import java.util.List;
import java.util.Locale;

import static es.udc.project.backend.rest.dtos.conversors.UserConversor.toAuthenticatedUserDto;
import static es.udc.project.backend.rest.dtos.conversors.UserConversor.toUser;
import static es.udc.project.backend.rest.dtos.conversors.UserConversor.toUserDto;
import static es.udc.project.backend.rest.dtos.conversors.UserListConversor.toUserListDtos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PasswordNotStrongException;
import es.udc.project.backend.model.services.InternalService;
import es.udc.project.backend.model.services.UserService;
import es.udc.project.backend.rest.common.JwtGenerator;
import es.udc.project.backend.rest.common.JwtInfo;
import es.udc.project.backend.rest.dtos.AuthenticatedUserDto;
import es.udc.project.backend.rest.dtos.BlockDto;
import es.udc.project.backend.rest.dtos.UserDto;
import es.udc.project.backend.rest.dtos.UserListDto;
import es.udc.project.backend.rest.dtos.params.ChangePasswordParamsDto;
import es.udc.project.backend.rest.dtos.params.LoginParamsDto;
import es.udc.project.backend.model.exceptions.IncorrectPasswordException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.exceptions.IncorrectLoginException;
import es.udc.project.backend.rest.common.ErrorsDto;


@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JwtGenerator jwtGenerator;

	@Autowired
	private UserService userService;
	
	@Autowired
	private InternalService internalService;
	
	private  static final String PASSWORD_NOT_STRONG_EXCEPTION = "project.exceptions.PasswordNotStrongException";
	private static final String INCORRECT_LOGIN_EXCEPTION = "project.exceptions.IncorrectLoginException";
	private static final String INCORRECT_PASSWORD_EXCEPTION = "project.exceptions.IncorrectPasswordException";
	
	
	
	@ExceptionHandler(PasswordNotStrongException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ErrorsDto handlePasswordNotStrongException(PasswordNotStrongException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(PASSWORD_NOT_STRONG_EXCEPTION, null,
				PASSWORD_NOT_STRONG_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	

	
	@ExceptionHandler(IncorrectLoginException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ErrorsDto handleIncorrectLoginException(IncorrectLoginException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(INCORRECT_LOGIN_EXCEPTION, null,
				INCORRECT_LOGIN_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	@ExceptionHandler(IncorrectPasswordException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ErrorsDto handleIncorrectPasswordException(IncorrectPasswordException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(INCORRECT_PASSWORD_EXCEPTION, null,
				INCORRECT_PASSWORD_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}

	@PostMapping("/signUp")
	public ResponseEntity<AuthenticatedUserDto> signUp(
			@Validated({ UserDto.AllValidations.class }) @RequestBody UserDto userDto)
			throws DuplicatedInstanceException, InstanceNotFoundException {
		System.out.println(userDto.getRole());
		internalService.checkRole(userDto.getRole());
		User user = toUser(userDto);
		userService.signUp(user,true);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId())
				.toUri();
		return ResponseEntity.created(location).body(toAuthenticatedUserDto(generateServiceToken(user), user));
	}
	
	@PostMapping("/login")
	public AuthenticatedUserDto login(@Validated @RequestBody LoginParamsDto params) throws IncorrectLoginException {
		User user = userService.login(params.getUserName(), params.getPassword());
		return toAuthenticatedUserDto(generateServiceToken(user), user);
	}
	@PostMapping("/loginFromServiceToken")
	public AuthenticatedUserDto loginFromServiceToken(@RequestAttribute Long userId,
			@RequestAttribute String serviceToken) throws InstanceNotFoundException {

		User user = userService.loginFromId(userId);

		return toAuthenticatedUserDto(serviceToken, user);

	}
	
	@PostMapping("/{id}/changePassword")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changePassword(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated @RequestBody ChangePasswordParamsDto params)
			throws PermissionException, InstanceNotFoundException, IncorrectPasswordException, PasswordNotStrongException {

		if (!id.equals(userId)) {
			throw new PermissionException();
		}

		userService.changePassword(id, params.getOldPassword(), params.getNewPassword());

	}
	
	
	@GetMapping("/users")
	public BlockDto<UserListDto> findUsers(@RequestAttribute Long userId,
			@RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException, PermissionException {

		Block<User> userBlock = userService.findUsers(userId, page, 10);
		
		return new BlockDto<>(toUserListDtos(userBlock.getItems()), userBlock.getExistMoreItems());
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateProfile(@RequestAttribute Long userId, @PathVariable Long id,
			@Validated({ UserDto.UpdateValidations.class }) @RequestBody UserDto userDto)
			throws InstanceNotFoundException, PermissionException {
		if (!internalService.isAdmin(userId)) {
			throw new PermissionException();
		}
				userService.updateProfile(id, userDto.getFirstName(), userDto.getLastName(), userDto.getEmail());

	}


	

	private String generateServiceToken(User user) {

		JwtInfo jwtInfo = new JwtInfo(user.getId(), user.getUserName(), user.getRole().toString());

		return jwtGenerator.generate(jwtInfo);

	}

}