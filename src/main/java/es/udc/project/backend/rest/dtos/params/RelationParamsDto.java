package es.udc.project.backend.rest.dtos.params;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.udc.project.backend.rest.dtos.RelationDto;
@XmlRootElement(name="relation")
public class RelationParamsDto {
	private RelationTargetParamsDto from;
	private RelationTargetParamsDto to;
	private RelationDto relation;
	//Status:
	// 0 -> persistent; 1 -> new; 2 -> deleted
	private int status;

	public RelationParamsDto() {
		
	}

	public RelationParamsDto(RelationTargetParamsDto from, RelationTargetParamsDto to, RelationDto relation,
			int status) {
		this.from = from;
		this.to = to;
		this.relation = relation;
		this.status = status;
	}
	
	public RelationTargetParamsDto getFrom() {
		return from;
	}

	public void setFrom(RelationTargetParamsDto from) {
		this.from = from;
	}

	public RelationTargetParamsDto getTo() {
		return to;
	}

	public void setTo(RelationTargetParamsDto to) {
		this.to = to;
	}
	@XmlElement(name="details")
	public RelationDto getRelation() {
		return relation;
	}

	public void setRelation(RelationDto relation) {
		this.relation = relation;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
