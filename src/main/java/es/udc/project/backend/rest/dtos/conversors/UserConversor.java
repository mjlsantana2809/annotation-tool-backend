package es.udc.project.backend.rest.dtos.conversors;

import es.udc.project.backend.model.entities.User.RoleType;
import es.udc.project.backend.rest.dtos.AuthenticatedUserDto;
import es.udc.project.backend.rest.dtos.UserDto;

import java.time.LocalDateTime;

import es.udc.project.backend.model.entities.User;

public class UserConversor {

	private UserConversor() {
	}

	public static final UserDto toUserDto(User user) {
		return new UserDto(user.getId(), user.getUserName(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getRole().toString(),user.isAutoGeneratedPassword());
	}

	public static final User toUser(UserDto userDto) {
		RoleType role=RoleType.valueOf(userDto.getRole().toUpperCase());
		return new User(userDto.getUserName(), userDto.getFirstName(), userDto.getLastName(),
				userDto.getEmail(),role,LocalDateTime.now());
	}

	public static final AuthenticatedUserDto toAuthenticatedUserDto(String serviceToken, User user) {

		return new AuthenticatedUserDto(serviceToken, toUserDto(user));

	}

}
