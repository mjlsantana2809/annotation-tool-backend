package es.udc.project.backend.rest.dtos.conversors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Fragment;
import es.udc.project.backend.model.entities.Group;
import es.udc.project.backend.rest.dtos.params.GroupParamsDto;
import es.udc.project.backend.rest.dtos.params.SegmentParamsDto;

public class GroupParamsConversor {
	
	public static final GroupParamsDto toGroupParamsDto(Group group) {
		if(group.getGroup()!=null) {
			return new GroupParamsDto(group.getId(),group.getId().toString(), group.getGroupLabel(), new ArrayList<SegmentParamsDto>() , group.getPosX(), group.getPosY(), group.getGroup().getId(),0);
		}else {
			return new GroupParamsDto(group.getId(),group.getId().toString(), group.getGroupLabel(), new ArrayList<SegmentParamsDto>() , group.getPosX(), group.getPosY(), null,0);
		}
		
	}

	public static final List<GroupParamsDto> toGroupParamsDtos(List<Group> groups) {
		return groups.stream().map(p -> toGroupParamsDto(p)).collect(Collectors.toList());
	}
}
