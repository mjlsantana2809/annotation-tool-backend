package es.udc.project.backend.rest.dtos.conversors;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.rest.dtos.PermittedCollectionsDto;

public class PermittedCollectionsConversor {
	
	public static final PermittedCollectionsDto toPermittedCollectionsDto(boolean fullInfo, Collection collection) {
		
		String[] path=collection.getPath().split("/");
		if(fullInfo) {
			return new PermittedCollectionsDto(collection.getId(),path[path.length-1], toMillis(collection.getDate()), collection.getPosts(),"WIP");
		}else {
			return new PermittedCollectionsDto(collection.getId(),path[path.length-1]);
		}
		
	}

	public static final List<PermittedCollectionsDto> toPermittedCollectionsDtos(boolean fullInfo,List<Collection> collections) {
		return collections.stream().map(p -> toPermittedCollectionsDto(fullInfo,p)).collect(Collectors.toList());
	}

	private static final long toMillis(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}
}
