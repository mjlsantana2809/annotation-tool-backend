package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.udc.project.backend.rest.dtos.params.GroupParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationParamsDto;
import es.udc.project.backend.rest.dtos.params.SegmentParamsDto;
@XmlRootElement(name="analysis")
public class SpeechAnalysisResultDto extends AnalysisDto {
	
	private Long methodologyId;
	private List<SegmentParamsDto> segments;
	private List<GroupParamsDto> groups;
	private List<RelationParamsDto> relations;
	
	public SpeechAnalysisResultDto() {

	}

	


	public SpeechAnalysisResultDto(Long id, PostDto post, LocalDateTime lastModification, boolean finished,Long methodologyId ,List<SegmentParamsDto> segments,
			List<GroupParamsDto> groups, List<RelationParamsDto> relations, Long userId) {
		super(id, post, lastModification, finished,userId);
		this.methodologyId=methodologyId;
		this.segments = segments;
		this.groups = groups;
		this.relations = relations;
	}


	public Long getMethodologyId() {
		return methodologyId;
	}
	
	public void setMethodologyId(Long methodologyId) {
		this.methodologyId = methodologyId;
	}
	@XmlElement(name="segment")
	public List<SegmentParamsDto> getSegments() {
		return segments;
	}


	public void setSegments(List<SegmentParamsDto> segments) {
		this.segments = segments;
	}


	public List<GroupParamsDto> getGroups() {
		return groups;
	}

	@XmlElement(name="group")
	public void setGroups(List<GroupParamsDto> groups) {
		this.groups = groups;
	}

	@XmlElement(name="relation")
	public List<RelationParamsDto> getRelations() {
		return relations;
	}


	public void setRelations(List<RelationParamsDto> relations) {
		this.relations = relations;
	}
	
	
	
	
}
