package es.udc.project.backend.rest.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private JwtGenerator jwtGenerator;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors().and().csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			.addFilter(new JwtFilter(authenticationManager(), jwtGenerator))
			.authorizeRequests()
			.antMatchers("/users/login").permitAll()
			.antMatchers("/users/loginFromServiceToken").permitAll()
			.antMatchers("/users/*/changePassword").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers("/users/signUp").hasRole("ADMIN")
			.antMatchers("/users/*").hasRole("ADMIN")
			.antMatchers("/collections/*/posts").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.POST,"/collections/*").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")	
			.antMatchers(HttpMethod.GET,"/collections/permittedCollections").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/collections/posts/*").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/collections/posts").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.POST,"/analysis").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.PUT,"/analysis").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/historical").hasAnyRole("ADMIN","LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/shared").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/sharedTo").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/*/export").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/*").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.POST,"/analysis/*/comment").hasRole("PSYCHOLOGIST")
			.antMatchers(HttpMethod.DELETE,"/analysis/*/comment/*").hasRole("PSYCHOLOGIST")
			.antMatchers(HttpMethod.POST,"/analysis/*/save").hasRole("PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/analysis/methodologies/*/relations").hasRole("LINGUIST")
			.antMatchers(HttpMethod.POST,"/analysis/*/saveSpeechAnalysis").hasRole("LINGUIST")
			.antMatchers(HttpMethod.POST,"/analysis/*/share").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.POST,"/analysis/*/unshare").hasAnyRole("LINGUIST","PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/statistics/countByPathology").hasRole("PSYCHOLOGIST")
			.antMatchers(HttpMethod.GET,"/statistics/relations").hasRole("LINGUIST")
			.antMatchers(HttpMethod.GET,"/statistics/relations/**").hasRole("LINGUIST")
			
			
			.anyRequest().hasRole("ADMIN");

	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		
		CorsConfiguration config = new CorsConfiguration();
	    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		
		config.setAllowCredentials(true);
	    config.addAllowedOrigin("*");
	    config.addAllowedHeader("*");
	    config.addAllowedMethod("*");
	    
	    source.registerCorsConfiguration("/**", config);
	    
	    return source;
	    
	 }

}
