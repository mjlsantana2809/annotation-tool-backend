package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Fragment;
import es.udc.project.backend.model.entities.Fragment_Fragment;
import es.udc.project.backend.model.entities.Fragment_Group;
import es.udc.project.backend.model.entities.Group;
import es.udc.project.backend.model.entities.Group_Group;
import es.udc.project.backend.rest.dtos.RelationDto;
import es.udc.project.backend.rest.dtos.params.GroupParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationTargetParamsDto;
import es.udc.project.backend.rest.dtos.params.SegmentParamsDto;

public class RelationParamsConversor {
	
	public static final RelationParamsDto toRelationParamsDto(Fragment_Fragment fxf) {
		RelationTargetParamsDto from = new RelationTargetParamsDto("segment", fxf.getFragmentFrom().getId().toString());
		RelationTargetParamsDto to = new RelationTargetParamsDto("segment", fxf.getFragmentTo().getId().toString());
		RelationDto relation = new RelationDto(fxf.getRelation().getId(), fxf.getRelation().getName(), fxf.getRelation().isBidirectional());
		return new RelationParamsDto(from, to, relation, 0);
	}

	public static final List<RelationParamsDto> fxfsToRelationParamsDtos(List<Fragment_Fragment> fxfs) {
		return fxfs.stream().map(p -> toRelationParamsDto(p)).collect(Collectors.toList());
	}
	
	public static final RelationParamsDto toRelationParamsDto(Fragment_Group fxg) {
		RelationTargetParamsDto group = new RelationTargetParamsDto("group", fxg.getGroup().getId().toString());
		RelationTargetParamsDto fragment = new RelationTargetParamsDto("segment",fxg.getFragment().getId().toString());
		RelationDto relation = new RelationDto(fxg.getRelation().getId(), fxg.getRelation().getName(), fxg.getRelation().isBidirectional());
		if(fxg.isFragmentSource()) {
			return new RelationParamsDto(fragment, group, relation, 0);
		}else {
			return new RelationParamsDto(group, fragment, relation, 0);
		}
		
	}

	public static final List<RelationParamsDto> fxgsToRelationParamsDtos(List<Fragment_Group> fxgs) {
		return fxgs.stream().map(p -> toRelationParamsDto(p)).collect(Collectors.toList());
	}
	
	public static final RelationParamsDto toRelationParamsDto(Group_Group gxg) {
		RelationTargetParamsDto from = new RelationTargetParamsDto("group", gxg.getGroupFrom().getId().toString());
		RelationTargetParamsDto to = new RelationTargetParamsDto("group", gxg.getGroupTo().getId().toString());
		RelationDto relation = new RelationDto(gxg.getRelation().getId(), gxg.getRelation().getName(), gxg.getRelation().isBidirectional());
		return new RelationParamsDto(from, to, relation, 0);
	}

	public static final List<RelationParamsDto> gxgsToRelationParamsDtos(List<Group_Group> gxgs) {
		return gxgs.stream().map(p -> toRelationParamsDto(p)).collect(Collectors.toList());
	}
}
