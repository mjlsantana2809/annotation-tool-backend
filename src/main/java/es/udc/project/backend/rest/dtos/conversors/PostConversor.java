package es.udc.project.backend.rest.dtos.conversors;

import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.rest.dtos.PostDto;

public class PostConversor {

	
	public static final PostDto toPostDto(Writing writing) {
		String[] path=writing.getCollection().getPath().split("/");
		return new PostDto(writing.getId(), writing.getTitle(),
				writing.getDate().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli(),
				writing.getText(), writing.getAuthor(), path[path.length-1]);
	}
	
	
}
