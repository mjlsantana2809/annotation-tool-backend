package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Fragment;
import es.udc.project.backend.model.entities.Group;
import es.udc.project.backend.rest.dtos.params.SegmentParamsDto;

public class SegmentParamsConversor {
	
	public static final SegmentParamsDto toSegmentParamsDto(Fragment fragment) {
		String group = null;
		if(fragment.getGroup()!=null) {
			group= fragment.getGroup().getId();
		}
		
		return new SegmentParamsDto(fragment.getId(),fragment.getId().toString(),fragment.getStart(), fragment.getEnd(), fragment.getFontColor(), fragment.getBackgroundColor(), fragment.getPosX(), fragment.getPosY(),group, 0,fragment.isTitle());
	}

	public static final List<SegmentParamsDto> toSegmentParamsDtos(List<Fragment> fragments) {
		return fragments.stream().map(p -> toSegmentParamsDto(p)).collect(Collectors.toList());
	}
}
