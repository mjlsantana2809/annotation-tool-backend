package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="analysis")
public class AnalysisDto {
	private Long id;
	private PostDto post;
	private long lastModification;
	private boolean finished;
	private Long userId;
	
	public AnalysisDto() {

	}
	
	public AnalysisDto(Long id, PostDto post, LocalDateTime lastModification, boolean finished, Long userId) {
		this.id = id;
		this.post = post;
		this.lastModification = toMillis(lastModification);
		this.finished=finished;
		this.userId=userId;
	}
	@XmlElement(name="id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@XmlElement(name="post")
	public PostDto getPost() {
		return post;
	}
	public void setPost(PostDto post) {
		this.post = post;
	}
	@XmlElement(name="lastModification")
	public long getLastModification() {
		return lastModification;
	}
	public void setLastModification(LocalDateTime lastModification) {
		this.lastModification = toMillis(lastModification);
	}
	@XmlElement(name="finished")
	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	@XmlElement(name="userId")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	private long toMillis(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
		
	}
	
	
}
