package es.udc.project.backend.rest.dtos;


public class PathologyCountDto {
	private String pathologyName;
	private int pathologyId;
	private long count;
	
	public PathologyCountDto() {

	}

	public PathologyCountDto(String pathologyName, int pathologyId, long count) {
		this.pathologyName = pathologyName;
		this.pathologyId = pathologyId;
		this.count = count;
	}

	public String getPathologyName() {
		return pathologyName;
	}

	public void setPathologyName(String pathologyName) {
		this.pathologyName = pathologyName;
	}

	public int getPathologyId() {
		return pathologyId;
	}

	public void setPathologyId(int pathologyId) {
		this.pathologyId = pathologyId;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
	
	
	
	
}
