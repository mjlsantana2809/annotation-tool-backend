package es.udc.project.backend.rest.dtos;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Comment")
public class CommentDto {
	private Long id;
	private  long start;
	private long end;
	private String text;

	private int status;
	private boolean title;
	
	
	public CommentDto() {
		
	}
	
	
	public CommentDto(Long id, long start, long end, String text, int status, boolean title) {
		this.id = id;
		this.start = start;
		this.end = end;
		this.text = text;
		this.status = status;
		this.title=title;
	}
	@XmlElement(name="Id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@XmlElement(name="Start")
	public long getStart() {
		return start;
	}
	public void setStart(long start) {
		this.start = start;
	}
	@XmlElement(name="End")
	public long getEnd() {
		return end;
	}
	public void setEnd(long end) {
		this.end = end;
	}
	@XmlElement(name="Text")
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}


	public boolean isTitle() {
		return title;
	}


	public void setTitle(boolean title) {
		this.title = title;
	}
	
	
	
	
}
