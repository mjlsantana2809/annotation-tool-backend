package es.udc.project.backend.rest.dtos;

public class PermittedCollectionsDto {
	private long id;
	private String name;
	private Long date;
	private Long posts;
	//Cambiarlo por int luego
	private String completion;
	
	public PermittedCollectionsDto(long id,String name, long date, long posts, String completion) {
		this.id= id;
		this.name = name;
		this.date = date;
		this.posts = posts;
		this.completion= completion;
	}
	
	

	public PermittedCollectionsDto(long id, String name) {
		this.id = id;
		this.name = name;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public Long getPosts() {
		return posts;
	}

	public void setPosts(Long posts) {
		this.posts = posts;
	}

	public String getCompletion() {
		return completion;
	}

	public void setCompletion(String completion) {
		this.completion = completion;
	}
	
	
}
