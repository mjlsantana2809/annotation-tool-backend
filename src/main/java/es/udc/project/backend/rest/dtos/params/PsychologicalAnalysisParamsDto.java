package es.udc.project.backend.rest.dtos.params;

import java.util.List;

public class PsychologicalAnalysisParamsDto {
	private String diagnosis;
	private List<CommentParamsDto> comments;
	private int pathology;
	
	public PsychologicalAnalysisParamsDto() {
	}

	public PsychologicalAnalysisParamsDto(String diagnosis, List<CommentParamsDto> comments, int pathology) {
		this.diagnosis = diagnosis;
		this.comments = comments;
		this.pathology=pathology;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public List<CommentParamsDto> getComments() {
		return comments;
	}

	public void setComments(List<CommentParamsDto> comments) {
		this.comments = comments;
	}

	public int getPathology() {
		return pathology;
	}

	public void setPathology(int pathology) {
		this.pathology = pathology;
	}
	
	
	
	
	
}
