package es.udc.project.backend.rest.dtos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="relationDetails")
public class RelationDto {
	private Long id;
	private String name;
	private boolean bidirectional;

	public RelationDto() {
		
	}
	public RelationDto(Long id,String name, boolean bidirectional) {
		this.id=id;
		this.name = name;
		this.bidirectional = bidirectional;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBidirectional() {
		return bidirectional;
	}

	public void setBidirectional(boolean bidirectional) {
		this.bidirectional = bidirectional;
	}
	
	
}
