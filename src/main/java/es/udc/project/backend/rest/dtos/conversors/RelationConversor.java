package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Relation_Type;
import es.udc.project.backend.rest.dtos.RelationDto;

public class RelationConversor {
	public static final RelationDto toRelationDto(Relation_Type relation) {
		return new RelationDto(relation.getId(),relation.getName(), relation.isBidirectional());
	}

	public static final List<RelationDto> toRelationDtos(List<Relation_Type> relations) {
		return relations.stream().map(r -> toRelationDto(r)).collect(Collectors.toList());
	}
}
