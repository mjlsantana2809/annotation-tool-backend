package es.udc.project.backend.rest.dtos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StatisticsDto {
	private List<PathologyCountDto> pathologyCount;
	private long totalCount;
	private List<RelationCountDto> relationCount;
	
	public StatisticsDto() {
	}
	
	
	public StatisticsDto(List<PathologyCountDto> pathologyCount, long totalCount) {
		this.pathologyCount = pathologyCount;
		this.totalCount = totalCount;
	}
	
	public StatisticsDto( long totalCount,List<RelationCountDto> relationCount) {
		this.relationCount = relationCount;
		this.totalCount = totalCount;
	}
	@JsonProperty("data")
	public List<PathologyCountDto> getPathologyCount() {
		return pathologyCount;
	}
	public void setPathologyCount(List<PathologyCountDto> pathologyCount) {
		this.pathologyCount = pathologyCount;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	
	@JsonProperty("relations")
	public List<RelationCountDto> getRelationCount() {
		return relationCount;
	}


	public void setRelationCount(List<RelationCountDto> relationCount) {
		this.relationCount = relationCount;
	}
	
	
	
	
}
