package es.udc.project.backend.rest.controllers;

import static es.udc.project.backend.rest.dtos.conversors.CollectionsConversor.toCollectionsDto;
import static es.udc.project.backend.rest.dtos.conversors.CollectionsConversor.toCollectionsDtos;
import static es.udc.project.backend.rest.dtos.conversors.PermittedCollectionsConversor.toPermittedCollectionsDtos;
import static es.udc.project.backend.rest.dtos.conversors.PostConversor.toPostDto;
import static es.udc.project.backend.rest.dtos.conversors.PostListConversor.toPostListDtos;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.model.entities.Writing;
import es.udc.project.backend.model.exceptions.EmptyCollectionException;
import es.udc.project.backend.model.exceptions.FileStorageException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.services.ApiResponse;
import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.services.CollectionService;
import es.udc.project.backend.model.services.FileStorageService;
import es.udc.project.backend.model.services.InternalService;
import es.udc.project.backend.rest.common.ErrorsDto;
import es.udc.project.backend.rest.dtos.BlockDto;
import es.udc.project.backend.rest.dtos.CollectionsDto;
import es.udc.project.backend.rest.dtos.PermittedCollectionsDto;
import es.udc.project.backend.rest.dtos.PostDto;
import es.udc.project.backend.rest.dtos.PostListDto;

@RestController
@RequestMapping("/collections")
public class CollectionController {
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
    private FileStorageService fileStorageService;
	
	@Autowired 
	private CollectionService collectionService;
	
	@Autowired
	private InternalService internalService;
	
	private static final  String EMPTY_COLLECTION_EXCEPTION = "project.exceptions.EmptyCollectionException";

	
	@ExceptionHandler(EmptyCollectionException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	public ErrorsDto handleEmptyCollectionException(EmptyCollectionException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(EMPTY_COLLECTION_EXCEPTION, null,
				EMPTY_COLLECTION_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	

    
    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, @RequestParam String collectionName) throws FileStorageException{
        String fileName = fileStorageService.storeFile(file,collectionName);
        return fileName;
    }
    
    @PostMapping("/uploadMultipleFiles")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void uploadMultipleFiles(@RequestParam("data") MultipartFile[] data, @RequestParam String collectionName)throws FileStorageException, NoSuchFileException, IOException {
         Collection collection=collectionService.createCollection(collectionName);
         String prueba=fileStorageService.createCollectionFolder(collectionName);
         System.out.println(prueba);
    	for(MultipartFile file:data) { 
        	 collectionService.toJavaObject(fileStorageService.storeFile(file,collectionName), collection);
         }
    }
    
    @GetMapping("")
    public BlockDto<CollectionsDto> findAllCollections(@RequestParam(defaultValue = "0") int page){
    	
    	Block<Collection> collectionBlock = collectionService.findAllCollections(page, 10);
		
    	return new BlockDto<>(toCollectionsDtos(collectionBlock.getItems()), collectionBlock.getExistMoreItems());
    }
    
    @GetMapping("/permittedCollections")
    public List<PermittedCollectionsDto> findPermittedCollections(@RequestAttribute Long userId, @RequestParam Long targetId, @RequestParam(defaultValue="true")boolean fullInfo) throws InstanceNotFoundException, PermissionException{
    	List<Collection> collections= new ArrayList<>();
    	
    	if(internalService.isAdmin(userId) || userId.equals(targetId)) {
    		collections = collectionService.findPermittedCollections(targetId);
    	}else {
    		throw new PermissionException();
    	}
    	
    	if(collections.isEmpty()) {
    		return new ArrayList<PermittedCollectionsDto>();
    	}else {
    		return new ArrayList<PermittedCollectionsDto>(toPermittedCollectionsDtos(fullInfo,collections));
    	}
    	
    }
    
    @GetMapping("/{collectionId}/posts")
    public BlockDto<PostListDto> findByCollectionId(@RequestAttribute Long userId,@PathVariable Long collectionId, @RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException, PermissionException{
    	Block<Writing> writingBlock = collectionService.findByCollectionId(userId,collectionId, page, 10);
    	return new BlockDto<>(toPostListDtos(writingBlock.getItems()),writingBlock.getExistMoreItems());
    }
    
    @PutMapping("/{collectionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCollectionStatus(@RequestParam boolean enabled, @PathVariable Long collectionId) throws InstanceNotFoundException {
    	collectionService.updateCollectionStatus(enabled, collectionId);
    	
    }
    
    @PostMapping("/{collectionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePermission(@RequestParam Long targetUserId, @PathVariable Long collectionId) throws InstanceNotFoundException {
    	collectionService.updatePermission(targetUserId, collectionId);
    	
    }
    
    @PostMapping("/redditAPI")
    public CollectionsDto importByKeywords( @RequestParam String subreddit, @RequestParam String keywords, @RequestParam String collectionName, @RequestParam(defaultValue="25") int size) throws EmptyCollectionException {
    
    	return toCollectionsDto(collectionService.importByKeywords(subreddit, keywords, collectionName, size));
    }
    
    @GetMapping("/posts/{postId}")
    public PostDto findPostById(@RequestAttribute Long userId, @PathVariable Long postId) throws InstanceNotFoundException, PermissionException {
    	return toPostDto(collectionService.findPostById(postId, userId));
    }
    
    @GetMapping("/posts")
    public BlockDto<PostListDto> findPostByAuthor(@RequestAttribute Long userId, @RequestParam String author,@RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException, PermissionException {
    	Block<Writing> writingBlock=collectionService.findPostsByAuthor(userId, author, page, 10);
    	return new BlockDto<>(toPostListDtos(writingBlock.getItems()),writingBlock.getExistMoreItems());
    }
    
}
