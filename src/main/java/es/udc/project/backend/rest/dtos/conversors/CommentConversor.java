package es.udc.project.backend.rest.dtos.conversors;


import java.util.List;
import java.util.stream.Collectors;


import es.udc.project.backend.model.entities.Comment;
import es.udc.project.backend.rest.dtos.CommentDto;

public class CommentConversor {
	public static final CommentDto toCommentDto(Comment comment) {
		return new CommentDto(comment.getId(), comment.getStart(), comment.getEnd(), comment.getText(),0, comment.isTitle());
	}

	public static final List<CommentDto> toCommentDtos(List<Comment> comments) {
		return comments.stream().map(e -> toCommentDto(e)).collect(Collectors.toList());
	}

}
