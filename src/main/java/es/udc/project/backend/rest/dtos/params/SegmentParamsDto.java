package es.udc.project.backend.rest.dtos.params;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="segment")
public class SegmentParamsDto {
	private String id;
	private String feId;
	private Long start;
	private Long end;
	private String fontColor;
	private String bgColor;
	private Long posX;
	private Long posY;
	private String group;
	//Status:
	// 0 -> persistent; 1 -> new; 2 -> deleted
	private int status;
	private boolean title;

	public SegmentParamsDto() {
		
	}
	
	
	public SegmentParamsDto(String id,String feId,Long start, Long end, String fontColor, String bgColor, Long posX, Long posY, String group,
			int status, boolean title) {
		this.id=id;
		this.feId= feId;
		this.start = start;
		this.end = end;
		this.fontColor = fontColor;
		this.bgColor = bgColor;
		this.posX = posX;
		this.posY = posY;
		this.group = group;
		this.status = status;
		this.title=title;
	}
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getFeId() {
		return feId;
	}

	public void setFeId(String feId) {
		this.feId = feId;
	}

	public Long getStart() {
		return start;
	}
	public void setStart(Long start) {
		this.start = start;
	}
	public Long getEnd() {
		return end;
	}
	public void setEnd(Long end) {
		this.end = end;
	}
	public String getFontColor() {
		return fontColor;
	}
	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}
	public Long getPosX() {
		return posX;
	}
	public void setPosX(Long posX) {
		this.posX = posX;
	}
	public Long getPosY() {
		return posY;
	}
	public void setPosY(Long posY) {
		this.posY = posY;
	}

	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isTitle() {
		return title;
	}

	public void setTitle(boolean title) {
		this.title = title;
	}
	
}
