package es.udc.project.backend.rest.dtos;

public class CollectionsDto {
	private long id;
	private String name;
	private long date;
	private long posts;
	private boolean enabled;
	
	public CollectionsDto(long id,String name, long date, long posts, boolean enabled) {
		this.id= id;
		this.name = name;
		this.date = date;
		this.posts = posts;
		this.enabled = enabled;
	}
	
	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public long getPosts() {
		return posts;
	}

	public void setPosts(long posts) {
		this.posts = posts;
	}



	public boolean isEnabled() {
		return enabled;
	}



	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
}
