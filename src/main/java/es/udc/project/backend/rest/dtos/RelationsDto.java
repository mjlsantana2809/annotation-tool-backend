package es.udc.project.backend.rest.dtos;

import java.util.List;

public class RelationsDto {
	private Long methodologyId;
	private List<RelationDto> relations;
	
	
	public RelationsDto() {
	}
	
	public RelationsDto(Long methodologyId, List<RelationDto> relations) {
		this.methodologyId = methodologyId;
		this.relations = relations;
	}
	public Long getMethodologyId() {
		return methodologyId;
	}
	public void setMethodologyId(Long methodologyId) {
		this.methodologyId = methodologyId;
	}
	public List<RelationDto> getRelations() {
		return relations;
	}
	public void setRelations(List<RelationDto> relations) {
		this.relations = relations;
	}
	
	
}
