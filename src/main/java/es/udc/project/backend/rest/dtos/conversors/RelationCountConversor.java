package es.udc.project.backend.rest.dtos.conversors;

import java.util.ArrayList;
import java.util.List;


import es.udc.project.backend.rest.dtos.RelationCountDto;

public class RelationCountConversor {

	public static final List<RelationCountDto> toRelationCountDtos(List<Object[]> data){
		List<RelationCountDto> relationCount= new ArrayList<>();
		for(Object[] result:data) {
			String relation= (String) result[0];
			long count= ((Number) result[1]).longValue();
			relationCount.add( new RelationCountDto(relation, count));
			
		}
		
		return joinResults(relationCount);
		
	}
	
	private static List<RelationCountDto> joinResults(List<RelationCountDto> list){
		List<RelationCountDto> aux = new ArrayList<RelationCountDto>();
		boolean found= false;
		for(RelationCountDto e:list) {
			found=false;
			for(RelationCountDto x:aux) {
				if(x.getRelation().equals(e.getRelation())) {
					x.setCount(e.getCount()+e.getCount());
					found=true;
				}
				
			}
			if(!found) {
				aux.add(e);
			}
		}
		return aux;
	}
	
}
