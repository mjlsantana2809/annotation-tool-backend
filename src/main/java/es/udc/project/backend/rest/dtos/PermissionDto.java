package es.udc.project.backend.rest.dtos;

public class PermissionDto {
	
	private AnalysisDto analysis;
	private String owner;
	private String sharedTo;
	private Long sharedToId;
	
	public PermissionDto(AnalysisDto analysis, String owner, Long sharedToId, String sharedTo) {
		this.analysis = analysis;
		this.owner = owner;
		this.sharedTo = sharedTo;
		this.sharedToId=sharedToId;
	}

	public AnalysisDto getAnalysis() {
		return analysis;
	}

	public void setAnalysis(AnalysisDto analysis) {
		this.analysis = analysis;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	

	public String getSharedTo() {
		return sharedTo;
	}

	public void setSharedTo(String sharedTo) {
		this.sharedTo = sharedTo;
	}

	public Long getSharedToId() {
		return sharedToId;
	}

	public void setSharedToId(Long sharedToId) {
		this.sharedToId = sharedToId;
	}
	
	
	
}
