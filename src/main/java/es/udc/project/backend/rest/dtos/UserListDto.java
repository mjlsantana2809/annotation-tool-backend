package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.util.List;

public class UserListDto {
	private long userId;
	private String username;
	private List<CollectionsDto> allowedCollections;
	private long lastActivity;
	private Long analysisNumber;
	private String email;
	private String role;
	private String firstName;
	private String lastName;
	
	
	public UserListDto() {
	}


	public UserListDto(long userId, String username, List<CollectionsDto> allowedCollections, long lastActivity,
			Long analysisNumber, String email, String role, String firstName, String lastName) {
		this.userId = userId;
		this.username = username;
		this.allowedCollections = allowedCollections;
		this.lastActivity = lastActivity;
		this.analysisNumber = analysisNumber;
		this.email = email;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
	}


	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	


	public List<CollectionsDto> getAllowedCollections() {
		return allowedCollections;
	}


	public void setAllowedCollections(List<CollectionsDto> allowedCollections) {
		this.allowedCollections = allowedCollections;
	}


	public long getLastActivity() {
		return lastActivity;
	}


	public void setLastActivity(long lastActivity) {
		this.lastActivity = lastActivity;
	}


	public Long getAnalysisNumber() {
		return analysisNumber;
	}


	public void setAnalysisNumber(Long analysisNumber) {
		this.analysisNumber = analysisNumber;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	

}
