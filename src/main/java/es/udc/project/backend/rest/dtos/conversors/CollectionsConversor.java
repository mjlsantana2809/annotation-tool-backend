package es.udc.project.backend.rest.dtos.conversors;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.rest.dtos.CollectionsDto;

public class CollectionsConversor {
	
	public static final CollectionsDto toCollectionsDto(Collection collection) {
		String[] path=collection.getPath().split("/");
		return new CollectionsDto(collection.getId(),path[path.length-1], toMillis(collection.getDate()), collection.getPosts(), collection.isEnabled());
	}

	public static final List<CollectionsDto> toCollectionsDtos(List<Collection> collections) {
		return collections.stream().map(p -> toCollectionsDto(p)).collect(Collectors.toList());
	}

	private static final long toMillis(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}
}
