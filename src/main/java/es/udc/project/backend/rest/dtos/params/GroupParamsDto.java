package es.udc.project.backend.rest.dtos.params;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="Group")
public class GroupParamsDto {
	private String id;
	private String feId;
	private String groupLabel;
	private List<SegmentParamsDto> fragments;
	private Long posX;
	private Long posY;
	private String groupId;
	
	//Status:
	// 0 -> persistent; 1 -> new; 2 -> deleted
	private int status;
	
	public GroupParamsDto() {
	
	}
	
	
	public GroupParamsDto(String id, String feId, String groupLabel, List<SegmentParamsDto> fragments, Long posX,
			Long posY,String groupId, int status) {
		this.id = id;
		this.feId = feId;
		this.groupLabel = groupLabel;
		this.fragments = fragments;
		this.posX = posX;
		this.posY = posY;
		this.status = status;
		this.groupId= groupId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFeId() {
		return feId;
	}
	public void setFeId(String feId) {
		this.feId = feId;
	}
	public String getGroupLabel() {
		return groupLabel;
	}
	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}
	public List<SegmentParamsDto> getFragments() {
		return fragments;
	}
	public void setFragments(List<SegmentParamsDto> fragments) {
		this.fragments = fragments;
	}
	public Long getPosX() {
		return posX;
	}
	public void setPosX(Long posX) {
		this.posX = posX;
	}
	public Long getPosY() {
		return posY;
	}
	public void setPosY(Long posY) {
		this.posY = posY;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}


	public String getGroupId() {
		return groupId;
	}


	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}


	


	
	
	
	
}
