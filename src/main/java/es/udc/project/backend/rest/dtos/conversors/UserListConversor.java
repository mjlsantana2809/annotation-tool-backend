package es.udc.project.backend.rest.dtos.conversors;

import static es.udc.project.backend.rest.dtos.conversors.CollectionsConversor.toCollectionsDto;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.entities.User;
import es.udc.project.backend.rest.dtos.CollectionsDto;
import es.udc.project.backend.rest.dtos.UserListDto;


public class UserListConversor {
	private UserListConversor() {
	}

	public static final UserListDto toUserListDto(User user) {
		
		List<CollectionsDto> allowedCollections = new ArrayList<>();  
		for(Permission permission:user.getPermissions()) {
			if(permission.getCollection()!=null) {
				Collection collection = permission.getCollection();
				allowedCollections.add(toCollectionsDto(collection));
			}
			
		}
		
		
		return new UserListDto(user.getId(),user.getUserName(), allowedCollections,
				toMillis(user.getLastActivity()), (long)0, user.getEmail(),user.getRole().name(),
				user.getFirstName(),user.getLastName());
	}

	public static final List<UserListDto> toUserListDtos(List<User> users) {
		return users.stream().map(p -> toUserListDto(p)).collect(Collectors.toList());
	}

	private static final long toMillis(LocalDateTime date) {
		return date.truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
	}
}
