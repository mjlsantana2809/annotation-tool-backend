package es.udc.project.backend.rest.dtos.conversors;

import java.util.List;
import java.util.stream.Collectors;

import es.udc.project.backend.model.entities.Analysis;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.rest.dtos.AnalysisDto;
import es.udc.project.backend.rest.dtos.PermissionDto;
import es.udc.project.backend.rest.dtos.PostDto;

public class PermissionConversor {
	
	public static final PermissionDto toPermissionDto(Permission permission) {
		Analysis analysis;
		if(permission.getPsychologicalAnalysis()!=null) {
			analysis= permission.getPsychologicalAnalysis();
		}else {
			analysis=permission.getSpeechAnalysis();
		}	
		
		AnalysisDto analysisDto = new AnalysisDto(analysis.getId(), new PostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished(), analysis.getUser().getId());
		return new PermissionDto(analysisDto, analysis.getUser().getUserName(),permission.getUser().getId(),permission.getUser().getUserName());
	}

	public static final List<PermissionDto> toPermissionDtos(List<Permission> permissions) {
		return permissions.stream().map(r -> toPermissionDto(r)).collect(Collectors.toList());
	}
}
