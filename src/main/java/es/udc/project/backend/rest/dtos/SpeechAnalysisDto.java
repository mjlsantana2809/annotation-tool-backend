package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class SpeechAnalysisDto extends AnalysisDto {
	
	private Long methodologyId;
	public SpeechAnalysisDto(Long id, PostDto post, LocalDateTime lastModification,boolean finished, Long methodologyId, Long userId) {
		super(id, post, lastModification, finished,userId);
		this.methodologyId=methodologyId;
	}
	public Long getMethodologyId() {
		return methodologyId;
	}
	public void setMethodologyId(Long methodologyId) {
		this.methodologyId = methodologyId;
	}
	
	
	
	
	

	
}
