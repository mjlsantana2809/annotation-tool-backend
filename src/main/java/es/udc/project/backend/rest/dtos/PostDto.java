package es.udc.project.backend.rest.dtos;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.udc.project.backend.model.entities.Collection;
import es.udc.project.backend.model.entities.Writing;

@XmlRootElement(name="post")
public class PostDto {
	private Long id;
	private String title;
	private long date;
	private String text;
	private String author;
	private String collectionName;
	
	
	public PostDto() {
	}
	
	

	public PostDto(Long id, String title, long date, String text, String author, String collectionName) {
		this.id = id;
		this.title = title;
		this.date = date;
		this.text = text;
		this.author = author;
		this.collectionName = collectionName;
	}
	
	public PostDto(Writing writing) {
		this.id = writing.getId();
		this.title = writing.getTitle();
		this.date = writing.getDate().truncatedTo(ChronoUnit.MINUTES).atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
		this.text = writing.getText();
		this.author = writing.getAuthor();
		
		String[] path=writing.getCollection().getPath().split("/");
		this.collectionName =  path[path.length-1];
	}


	@XmlElement(name="Id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@XmlElement(name="Title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	@XmlElement(name="Date")
	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}
	@XmlElement(name="Text")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	@XmlElement(name="Author")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	@XmlElement(name="collectionName")
	public String getCollectionName() {
		return collectionName;
	}



	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
	
	
	
	
}
