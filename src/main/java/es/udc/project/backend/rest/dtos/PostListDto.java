package es.udc.project.backend.rest.dtos;

public class PostListDto {
	private long postId;
	private String postTitle;
	private String author;
	private int status;
	private String collectionName;
	private long collectionId;
	
	
	public PostListDto(long postId, String postTitle, String author, int status, String collectionName,
			long collectionId) {
		super();
		this.postId = postId;
		this.postTitle = postTitle;
		this.author = author;
		this.status = status;
		this.collectionName = collectionName;
		this.collectionId = collectionId;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public long getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(long collectionId) {
		this.collectionId = collectionId;
	}
	
	
	
	
	
}
