package es.udc.project.backend.rest.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import static es.udc.project.backend.rest.dtos.conversors.CommentConversor.toCommentDtos;
import static es.udc.project.backend.rest.dtos.conversors.PostConversor.toPostDto;
import static es.udc.project.backend.rest.dtos.conversors.RelationConversor.toRelationDtos;
import static es.udc.project.backend.rest.dtos.conversors.SegmentParamsConversor.toSegmentParamsDtos;
import static es.udc.project.backend.rest.dtos.conversors.GroupParamsConversor.toGroupParamsDtos;
import static es.udc.project.backend.rest.dtos.conversors.RelationParamsConversor.fxfsToRelationParamsDtos;
import static es.udc.project.backend.rest.dtos.conversors.RelationParamsConversor.fxgsToRelationParamsDtos;
import static es.udc.project.backend.rest.dtos.conversors.RelationParamsConversor.gxgsToRelationParamsDtos;
import static es.udc.project.backend.rest.dtos.conversors.AnalysisConversor.psychologicalAnalysisToAnalysisDtos;
import static es.udc.project.backend.rest.dtos.conversors.AnalysisConversor.speechAnalysisToAnalysisDtos;
import static es.udc.project.backend.rest.dtos.conversors.PermissionConversor.toPermissionDtos;

import es.udc.project.backend.model.entities.Psychological_Analysis;

import es.udc.project.backend.model.entities.Speech_Analysis;

import es.udc.project.backend.model.entities.Methodology;
import es.udc.project.backend.model.entities.Permission;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyFinishedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadySharedException;
import es.udc.project.backend.model.exceptions.AnalysisAlreadyStartedException;
import es.udc.project.backend.model.exceptions.CommentOutOfBoundsException;
import es.udc.project.backend.model.exceptions.DuplicatedInstanceException;
import es.udc.project.backend.model.exceptions.InstanceNotFoundException;
import es.udc.project.backend.model.exceptions.InvalidInputException;
import es.udc.project.backend.model.exceptions.MethodologyRequiredException;
import es.udc.project.backend.model.exceptions.PermissionException;
import es.udc.project.backend.model.services.AnalysisService;
import es.udc.project.backend.model.services.Block;
import es.udc.project.backend.model.services.InternalService;
import es.udc.project.backend.model.services.SpeechAnalysisResult;
import es.udc.project.backend.rest.common.ErrorsDto;
import es.udc.project.backend.rest.dtos.PsychologicalAnalysisDto;
import es.udc.project.backend.rest.dtos.AnalysisDto;
import es.udc.project.backend.rest.dtos.BlockDto;
import es.udc.project.backend.rest.dtos.IdDto;
import es.udc.project.backend.rest.dtos.PermissionDto;
import es.udc.project.backend.rest.dtos.PostDto;
import es.udc.project.backend.rest.dtos.RelationsDto;
import es.udc.project.backend.rest.dtos.SpeechAnalysisDto;
import es.udc.project.backend.rest.dtos.SpeechAnalysisResultDto;
import es.udc.project.backend.rest.dtos.params.CommentParamsDto;
import es.udc.project.backend.rest.dtos.params.PsychologicalAnalysisParamsDto;
import es.udc.project.backend.rest.dtos.params.RelationParamsDto;
import es.udc.project.backend.rest.dtos.params.SpeechAnalysisParamsDto;

@RestController
@RequestMapping("/analysis")
public class AnalysisController {
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private AnalysisService analysisService;
	
	@Autowired
	private InternalService internalService;
	
	private static final  String ANALYSIS_ALREADY_STARTED_EXCEPTION = "project.exceptions.AnalysisAlreadyStartedException";
	private static final  String ANALYSIS_ALREADY_FINISHED_EXCEPTION = "project.exceptions.AnalysisAlreadyFinishedException";
	private static final  String COMMENT_OUT_OF_BOUNDS_EXCEPTION = "project.exceptions.CommentOutOfBoundsException";
	private static final String METHODOLOGY_REQUIRED_EXCEPTION ="project.exceptions.MethodologyRequiredException";
	private static final String ANALYSIS_ALREADY_SHARED_EXCEPTION="project.exceptions.AnalysisAlreadySharedException";
	
	
	@ExceptionHandler(AnalysisAlreadySharedException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	@ResponseBody
	public ErrorsDto handleAnalysisAlreadySharedException(AnalysisAlreadySharedException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(ANALYSIS_ALREADY_SHARED_EXCEPTION, null,
				ANALYSIS_ALREADY_SHARED_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	
	@ExceptionHandler(AnalysisAlreadyStartedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorsDto handleAnalysisAlreadyStartedException(AnalysisAlreadyStartedException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(ANALYSIS_ALREADY_STARTED_EXCEPTION, null,
				ANALYSIS_ALREADY_STARTED_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	@ExceptionHandler(AnalysisAlreadyFinishedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorsDto handleAnalysisAlreadyFinishedException(AnalysisAlreadyFinishedException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(ANALYSIS_ALREADY_FINISHED_EXCEPTION, null,
				ANALYSIS_ALREADY_FINISHED_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	@ExceptionHandler(CommentOutOfBoundsException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ResponseBody
	public ErrorsDto handleCommentOutOfBoundsException(CommentOutOfBoundsException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(COMMENT_OUT_OF_BOUNDS_EXCEPTION, null,
				COMMENT_OUT_OF_BOUNDS_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	@ExceptionHandler(MethodologyRequiredException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorsDto handleMethodologyRequiredException(MethodologyRequiredException exception, Locale locale) {

		String errorMessage = messageSource.getMessage(METHODOLOGY_REQUIRED_EXCEPTION, null,
				METHODOLOGY_REQUIRED_EXCEPTION, locale);

		return new ErrorsDto(errorMessage);

	}
	
	private List<RelationParamsDto> fillRelations(SpeechAnalysisResult analysisResult) {
		List<RelationParamsDto> relations = new ArrayList<>();
		relations.addAll(fxfsToRelationParamsDtos(analysisResult.getFxf()));
    	relations.addAll(fxgsToRelationParamsDtos(analysisResult.getFxg()));
    	relations.addAll(gxgsToRelationParamsDtos(analysisResult.getGxg()));
    	return relations;
	}
	   
    @PostMapping("")
    public IdDto startAnalysis(@RequestAttribute Long userId, @RequestParam Long postId,@RequestParam(required = false) Long methodologyId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyStartedException, MethodologyRequiredException {
    	if(internalService.isPsychologist(userId)) {
    		return new IdDto(analysisService.startAnalysis(postId, userId));
    	}else if(internalService.isLinguist(userId)) {
    		if(methodologyId!=null) {
    			return new IdDto(analysisService.startAnalysis(postId, userId, methodologyId));
    		}else {
    			throw new MethodologyRequiredException();
    		}
    			
    	}else {
    		throw new PermissionException();
    	}
    		
    }
    
    @PutMapping("")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void finishAnalysis(@RequestAttribute Long userId, @RequestParam Long analysisId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
    		analysisService.finishAnalysis(analysisId, userId);
    	
    }
   
    
    @GetMapping("/{postId}")
    public AnalysisDto continueAnalysis(@RequestAttribute Long userId, @PathVariable Long postId, @RequestParam(required = false) Long targetUserId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
    	if(internalService.isPsychologist(userId)) {
    		
    		Psychological_Analysis analysis= analysisService.continuePsychologicalAnalysis(postId, userId,targetUserId);
    		
	    	
	    	if(analysis==null) {
	    		return null;
	    	}
	    	PostDto postDto = toPostDto( analysis.getWriting());
	    	return new PsychologicalAnalysisDto(analysis.getId(), postDto, analysis.getLastModification(), analysis.isFinished(), analysis.getDiagnosis(),toCommentDtos(analysis.getComments()), analysis.getUser().getId(), analysis.getPathology().ordinal());
	    	
	    	
    	}else if(internalService.isLinguist(userId)) {
    		SpeechAnalysisResult analysisResult = analysisService.continueSpeechAnalysis(postId, userId,targetUserId);
    		if( analysisResult==null) {
	    		return null;
	    	}
    		if( analysisResult.getAnalysis()==null) {
	    		return null;
	    	}
    		Speech_Analysis analysis= analysisResult.getAnalysis();
    		List<RelationParamsDto> relations = fillRelations(analysisResult);
	    	
	    	return new SpeechAnalysisResultDto(analysis.getId(), toPostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished(),
	    				analysis.getMethodology().getId(),toSegmentParamsDtos(analysisResult.getFragments()),toGroupParamsDtos(analysisResult.getGroups()), relations, analysis.getUser().getId());
    	}else {
    		throw new PermissionException();
    	}
    	
    }
 
    
    @DeleteMapping("/{analysisId}/comment/{commentId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteComment(@RequestAttribute Long userId, @PathVariable Long analysisId,@PathVariable Long commentId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
    	analysisService.deleteComment(userId, analysisId, commentId);
    }
    @PostMapping("/{analysisId}/save")
    public AnalysisDto saveAnalysis(@RequestAttribute Long userId, @PathVariable Long analysisId, @RequestBody PsychologicalAnalysisParamsDto analysisParams) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException {
		
			Psychological_Analysis analysis=analysisService.saveAnalysis(userId, analysisId, analysisParams);
	    	return new PsychologicalAnalysisDto(analysis.getId(),toPostDto(analysis.getWriting()),analysis.getLastModification(), analysis.isFinished(),analysis.getDiagnosis(), toCommentDtos(analysis.getComments()),analysis.getUser().getId(), analysis.getPathology().ordinal());
		
    	
    }
    
    @GetMapping("/methodologies")
    public List<Methodology> findMethodologies(@RequestAttribute Long userId) throws InstanceNotFoundException, PermissionException{
    	if(internalService.isPsychologist(userId)) {
    		throw new PermissionException();
    	}
    		return analysisService.findMethodologies();
    	
		
    	
    }
    
    @GetMapping("/methodologies/{methodologyId}/relations")
    public RelationsDto findRelationsByMethodologyId(@RequestAttribute Long userId, @PathVariable Long methodologyId){
    		
    		return new RelationsDto(methodologyId, toRelationDtos(analysisService.findRelations(methodologyId)));	
    }
    
    @PostMapping("/{analysisId}/saveSpeechAnalysis")
    public AnalysisDto saveAnalysis(@RequestAttribute Long userId, @PathVariable Long analysisId, @RequestBody SpeechAnalysisParamsDto analysisParams) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException, DuplicatedInstanceException, InvalidInputException {
		
			Speech_Analysis analysis=analysisService.saveAnalysis(userId, analysisId, analysisParams);
	    	return new SpeechAnalysisDto(analysis.getId(), toPostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished() ,analysis.getMethodology().getId(),analysis.getUser().getId());
		
    	
    }
    
    @GetMapping("/historical")
    public BlockDto<AnalysisDto> findAnalysisByUser(@RequestAttribute Long userId, @RequestParam Long targetUserId, 
    			@RequestParam(required=false, defaultValue="false") boolean showFinished, @RequestParam(defaultValue = "0") int page) 
    					throws InstanceNotFoundException, PermissionException {
    
    	if(!userId.equals(targetUserId) && !internalService.isAdmin(userId)) {
    		throw new PermissionException();
    	}
    	if(internalService.isLinguist(targetUserId)) {
    		Block<Speech_Analysis> result=analysisService.findSpeechAnalysisByUser(targetUserId, showFinished,page,10);
    		
	    	return new BlockDto<>(speechAnalysisToAnalysisDtos(result.getItems()),result.getExistMoreItems());
    	}else if(internalService.isPsychologist(targetUserId)){   
    		Block<Psychological_Analysis> result=analysisService.findPsychologicalAnalysisByUser(targetUserId, showFinished,page,10);
    		
    		return new BlockDto<>(psychologicalAnalysisToAnalysisDtos(result.getItems()),result.getExistMoreItems());
    	}else {
    		throw new PermissionException();
    	}
    	
    }
    
    @PostMapping("/{analysisId}/share")
    public IdDto shareAnalysis(@RequestAttribute Long userId, @PathVariable Long analysisId, @RequestParam String targetUsername) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException, DuplicatedInstanceException, InvalidInputException, AnalysisAlreadySharedException {
    	return new IdDto(analysisService.shareAnalysis(userId, targetUsername, analysisId));
    }
    @PostMapping("/{analysisId}/unshare")
    @ResponseStatus(HttpStatus.OK)
    public void unshareAnalysis(@RequestAttribute Long userId, @PathVariable Long analysisId, @RequestParam Long targetUserId) throws InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException, CommentOutOfBoundsException, DuplicatedInstanceException, InvalidInputException {
		analysisService.unshareAnalysis(userId,analysisId,targetUserId);
    }
    
    @GetMapping("/shared")
    public BlockDto<PermissionDto> findsharedAnalysis(@RequestAttribute Long userId, @RequestParam(defaultValue = "0") int page ) throws InstanceNotFoundException {
   
		Block<Permission> permissionBlock= analysisService.findMySharedAnalysis(userId,page,10);
    	return new BlockDto<>(toPermissionDtos(permissionBlock.getItems()),permissionBlock.getExistMoreItems());
    }
    
    @GetMapping("/sharedTo")
    public BlockDto<PermissionDto> findSharedAnalysisTo(@RequestAttribute Long userId, @RequestParam Long targetUserId, @RequestParam(defaultValue = "0") int page) throws InstanceNotFoundException{	
    	
    	Block<Permission> permissionBlock= analysisService.findSharedAnalysisTo(targetUserId,page,10);
    	return new BlockDto<>(toPermissionDtos(permissionBlock.getItems()),permissionBlock.getExistMoreItems());
    }
   
    @GetMapping(value="/{postId}/export", produces=MediaType.TEXT_XML_VALUE)
    public @ResponseBody AnalysisDto downloadXML(@RequestAttribute Long userId,@PathVariable Long postId) throws   InstanceNotFoundException, PermissionException, AnalysisAlreadyFinishedException {
    	
        if(internalService.isPsychologist(userId)) {
        	Psychological_Analysis analysis= analysisService.continuePsychologicalAnalysis(postId, userId,null);
         	if(analysis==null) {
         		return null;
         	}
         	PostDto postDto = toPostDto( analysis.getWriting());
         	
         	return new PsychologicalAnalysisDto(analysis.getId(), postDto, analysis.getLastModification(), analysis.isFinished(), analysis.getDiagnosis(), toCommentDtos(analysis.getComments()), analysis.getUser().getId(), analysis.getPathology().ordinal());
        
        }else {
        	SpeechAnalysisResult analysisResult = analysisService.continueSpeechAnalysis(postId, userId,null);
    		if( analysisResult==null ||analysisResult.getAnalysis()==null) {
	    		return null;
	    	}
        	Speech_Analysis analysis= analysisResult.getAnalysis();
    		
        	return new SpeechAnalysisResultDto(analysis.getId(), toPostDto(analysis.getWriting()), analysis.getLastModification(), analysis.isFinished(),
    				analysis.getMethodology().getId(),toSegmentParamsDtos(analysisResult.getFragments()),toGroupParamsDtos(analysisResult.getGroups()), fillRelations(analysisResult), analysis.getUser().getId());
        }
       
    	
    }
    
}
