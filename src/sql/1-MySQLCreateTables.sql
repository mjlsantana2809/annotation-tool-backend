-- Indexes for primary keys have been explicitly created.

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS Psychological_Analysis;
DROP TABLE IF EXISTS Permission;
DROP TABLE IF EXISTS Writing;
DROP TABLE IF EXISTS Collection;
DROP TABLE IF EXISTS Comment;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Methodology;
DROP TABLE IF EXISTS Speech_Analysis;
DROP TABLE IF EXISTS Relation_Type;
DROP TABLE IF EXISTS FGroup;
DROP TABLE IF EXISTS Fragment;
DROP TABLE IF EXISTS Group_Group;
DROP TABLE IF EXISTS Fragment_Group;
DROP TABLE IF EXISTS Fragment_Fragment;


SET FOREIGN_KEY_CHECKS=1;


CREATE TABLE Collection (
    id BIGINT NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    path TEXT NOT NULL, 
    enabled TINYINT(1) NOT NULL,
    posts BIGINT NOT NULL,
    CONSTRAINT CollectionPK PRIMARY KEY (id)
) ENGINE = InnoDB;


CREATE TABLE User (
    id BIGINT NOT NULL AUTO_INCREMENT,
    userName VARCHAR(60)  NOT NULL,
    password VARCHAR(60) NOT NULL, 
    firstName VARCHAR(60) NOT NULL,
    lastName VARCHAR(60) NOT NULL, 
    email VARCHAR(60) NOT NULL,
    role TINYINT NOT NULL,
    lastActivity DATETIME NOT NULL,
    autoGeneratedPassword TINYINT(1) NOT NULL,
    CONSTRAINT UserPK PRIMARY KEY (id),
    CONSTRAINT UserNameUniqueKey UNIQUE (userName)
) ENGINE = InnoDB;

CREATE INDEX UserIndexByUserName ON User (userName);



CREATE TABLE Writing (
    id BIGINT NOT NULL AUTO_INCREMENT,
    title TEXT NOT NULL,
    date DATETIME NOT NULL, 
    info VARCHAR(100) NOT NULL,
    text MEDIUMTEXT NOT NULL,
    author VARCHAR(100) NOT NULL,
    collection BIGINT NOT NULL,
    CONSTRAINT WritingPK PRIMARY KEY (id),
    CONSTRAINT WritingCollectionFK FOREIGN KEY(collection) REFERENCES Collection (id)
) ENGINE = InnoDB;

CREATE TABLE Psychological_Analysis (
    id BIGINT NOT NULL AUTO_INCREMENT,
    startDate DATETIME NOT NULL,
    lastModification DATETIME NOT NULL, 
    diagnosis TINYTEXT NOT NULL,
    finished TINYINT(1) NOT NULL,
    user BIGINT NOT NULL,
    writing BIGINT NOT NULL,
    pathology TINYINT NOT NULL,
    CONSTRAINT Psychological_AnalysisPK PRIMARY KEY (id),
    CONSTRAINT PsyUserFK FOREIGN KEY(user) REFERENCES User (id),
    CONSTRAINT PsyWritingFK FOREIGN KEY(writing) REFERENCES Writing (id)
) ENGINE = InnoDB;

CREATE TABLE Comment (
    id BIGINT NOT NULL AUTO_INCREMENT,
    start BIGINT NOT NULL,
    end BIGINT NOT NULL, 
    text TINYTEXT NOT NULL,
    analysis BIGINT NOT NULL,
    title TINYINT(1) NOT NULL,
    CONSTRAINT CommentPK PRIMARY KEY (id),
    CONSTRAINT CommentAnalysisFK FOREIGN KEY(analysis) REFERENCES Psychological_Analysis (id)
) ENGINE = InnoDB;

CREATE TABLE Methodology (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    description TINYTEXT NOT NULL, 
    CONSTRAINT MethodologyPK PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE Speech_Analysis (
    id BIGINT NOT NULL AUTO_INCREMENT,
    startDate DATETIME NOT NULL,
    lastModification DATETIME NOT NULL, 
    finished TINYINT(1) NOT NULL,
    user BIGINT NOT NULL,
    writing BIGINT NOT NULL,
    methodology BIGINT NOT NULL,
    CONSTRAINT SpeechPK PRIMARY KEY (id),
    CONSTRAINT SpeechUserFK FOREIGN KEY(user) REFERENCES User (id),
    CONSTRAINT SpeechWritingFK FOREIGN KEY(writing) REFERENCES Writing (id),
    CONSTRAINT SpeechMethodologyFK FOREIGN KEY(methodology) REFERENCES Methodology (id)
) ENGINE = InnoDB;

CREATE TABLE Permission (
    id BIGINT NOT NULL AUTO_INCREMENT,
    date DATETIME NOT NULL,
    type VARCHAR(30) NOT NULL, 
    user BIGINT NOT NULL,
    collection BIGINT,
    psychologicalAnalysis BIGINT,
    speechAnalysis BIGINT,
    CONSTRAINT PermissionPK PRIMARY KEY (id),
    CONSTRAINT PermissionUserFK FOREIGN KEY(user) REFERENCES User (id),
    CONSTRAINT PermissionCollectionFK FOREIGN KEY(collection) REFERENCES Collection (id),
    CONSTRAINT PermissionPsychologicalAnalysisFK FOREIGN KEY(psychologicalAnalysis) REFERENCES Psychological_Analysis (id),
    CONSTRAINT PermissionSpeechAnalysisFK FOREIGN KEY(speechAnalysis) REFERENCES Speech_Analysis (id)
) ENGINE = InnoDB;

CREATE TABLE Relation_Type (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    bidirectional TINYINT NOT NULL, 
    methodology BIGINT NOT NULL,
    CONSTRAINT RelationPK PRIMARY KEY (id),
    CONSTRAINT RelationMethodologyFK FOREIGN KEY(methodology) REFERENCES Methodology (id)
) ENGINE = InnoDB;

CREATE TABLE FGroup (
    id VARCHAR(36) NOT NULL,
    analysis BIGINT NOT NULL,
    groupLabel TINYTEXT NOT NULL,
    posX BIGINT NOT NULL,
    posY BIGINT NOT NULL,
    fGroup VARCHAR(36),
    CONSTRAINT GroupPK PRIMARY KEY (id),
    CONSTRAINT GroupGroupFK FOREIGN KEY(fGroup) REFERENCES FGroup (id),
    CONSTRAINT FGroupAnalysisFK FOREIGN KEY(analysis) REFERENCES Speech_Analysis (id)
) ENGINE = InnoDB;

CREATE TABLE Fragment (
    id VARCHAR(36) NOT NULL,
    start BIGINT NOT NULL,
    end BIGINT NOT NULL, 
    fontColor VARCHAR(30) NOT NULL,
    backgroundColor VARCHAR(30) NOT NULL,
    analysis BIGINT NOT NULL,
    fGroup VARCHAR(36),
    posX BIGINT NOT NULL,
    posY BIGINT NOT NULL,
    title TINYINT(1) NOT NULL,
    CONSTRAINT FragmentPK PRIMARY KEY (id),
    CONSTRAINT FragmentAnalysisFK FOREIGN KEY(analysis) REFERENCES Speech_Analysis (id),
    CONSTRAINT FragmentGroupFK FOREIGN KEY(fGroup) REFERENCES FGroup (id)
) ENGINE = InnoDB;

CREATE TABLE Group_Group (
    id BIGINT NOT NULL AUTO_INCREMENT,
    groupFrom VARCHAR(36) NOT NULL,
    groupTo VARCHAR(36) NOT NULL, 
    relation BIGINT NOT NULL,
    CONSTRAINT GGPK PRIMARY KEY (id),
    CONSTRAINT GGGroupFromFK FOREIGN KEY(groupFrom) REFERENCES FGroup (id),
    CONSTRAINT GGGroupToFK FOREIGN KEY(groupTo) REFERENCES FGroup (id),
    CONSTRAINT GGRelationFK FOREIGN KEY(relation) REFERENCES Relation_Type (id)
) ENGINE = InnoDB;

CREATE TABLE Fragment_Group (
    id BIGINT NOT NULL AUTO_INCREMENT,
    fragment VARCHAR(36) NOT NULL,
    fGroup VARCHAR(36) NOT NULL, 
    fragmentSource TINYINT(1) NOT NULL,
    relation BIGINT NOT NULL,
    CONSTRAINT FGPK PRIMARY KEY (id),
    CONSTRAINT FGFragment FOREIGN KEY(fragment) REFERENCES Fragment (id),
    CONSTRAINT FGGroup FOREIGN KEY(fGroup) REFERENCES FGroup (id),
    CONSTRAINT FGRelation FOREIGN KEY(relation) REFERENCES Relation_Type (id)
) ENGINE = InnoDB;

CREATE TABLE Fragment_Fragment (
    id BIGINT NOT NULL AUTO_INCREMENT,
    fragmentFrom VARCHAR(36) NOT NULL,
    fragmentTo VARCHAR(36) NOT NULL, 
    relation BIGINT NOT NULL,
    CONSTRAINT FFPK PRIMARY KEY (id),
    CONSTRAINT FFFragmentFromFK FOREIGN KEY(fragmentFrom) REFERENCES Fragment (id),
    CONSTRAINT FFFragmentToFK FOREIGN KEY(fragmentTo) REFERENCES Fragment (id),
    CONSTRAINT FFRelationFK FOREIGN KEY(relation) REFERENCES Relation_Type (id)
) ENGINE = InnoDB;

